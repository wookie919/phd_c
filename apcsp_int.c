#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

typedef struct _EDGE {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int capacity;
} EDGE;

typedef struct _EDGE_LL {
	EDGE* edge;
	struct _EDGE_LL* next;
} EDGE_LL;

typedef struct _PSEUDO_EDGE {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int capacity;
	unsigned int distance;
	struct _PSEUDO_EDGE* edgeOne;
	struct _PSEUDO_EDGE* edgeTwo;
	struct _PSEUDO_EDGE_DLL* DLL;
} PSEUDO_EDGE;

typedef struct _PSEUDO_EDGE_DLL {
	struct _PSEUDO_EDGE* pseudoEdge;
	struct _PSEUDO_EDGE_DLL* prev;
	struct _PSEUDO_EDGE_DLL* next;
} PSEUDO_EDGE_DLL;

typedef struct _FLOW {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int bandwidth;
} FLOW;

typedef struct _FLOW_LL {
	FLOW* flow;
	struct _FLOW_LL* next;
} FLOW_LL;

typedef struct _NODE {
	unsigned int id;
	unsigned int numToEdges;
	unsigned int numFrEdges;
	EDGE** toEdges;
	EDGE** frEdges;
	struct _NODE* parent;
} NODE;

typedef struct _NODE_DLL {
	NODE* node;
	struct _NODE_DLL* prev;
	struct _NODE_DLL* next;
} NODE_DLL;

typedef struct _PATH_NODE {
	unsigned int nodeId;
	struct _PATH_NODE* next;
} PATH_NODE;

typedef struct _PATH {
	unsigned int distance;
	struct _PATH_NODE* start;
	struct _PATH_NODE* end;
} PATH;

typedef struct _PATH_CHAIN {
	struct _PATH_NODE* first;
	struct _PATH_NODE* last;
} PATH_CHAIN;

static unsigned int NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS;
static EDGE*** E;
static FLOW*** F;
static NODE** NODES;
static EDGE_LL** EDGE_BUCKET;
static FLOW_LL** FLOW_BUCKET;





/******************************************************************************/

void simple_algorithm(unsigned int SRC, PATH** PATH) {

	int i;
	unsigned int cap;
	unsigned int dstNodeId;
	unsigned char* IN_Q;
	NODE_DLL* Q_HEAD;
	NODE_DLL* Q_TAIL;
	NODE* node;
	NODE* parent;
	FLOW_LL* flowLL;
	FLOW* flow;
	PATH_NODE* pathNode;
	NODE_DLL* nodeDLL;

	for (cap = MAX_CAP; cap > 0; cap--) {
		IN_Q = (unsigned char*) malloc(NUM_NODES * sizeof(unsigned char));
		for (i = 0; i < NUM_NODES; i++) {
			IN_Q[i] = FALSE;
		}
		IN_Q[SRC] = TRUE;
		nodeDLL = (NODE_DLL*) malloc(sizeof(nodeDLL));
		nodeDLL->node = NODES[SRC];
		nodeDLL->next = NULL;
		nodeDLL->prev = NULL;
		Q_HEAD = nodeDLL;
		Q_TAIL = nodeDLL;
		while (Q_HEAD != NULL) {
			nodeDLL = Q_HEAD;
			Q_HEAD = nodeDLL->next;
			if (Q_HEAD == NULL) {
				Q_TAIL = NULL;
			}
			else {
				Q_HEAD->prev = NULL;
			}
			nodeDLL->next = NULL;
			nodeDLL->prev = NULL;
			node = nodeDLL->node;
			free(nodeDLL);
			for (i = 0; i < node->numToEdges; i++) {
				dstNodeId = node->toEdges[i]->dstNodeId;
				if (node->toEdges[i]->capacity >= cap) {
					if (!IN_Q[dstNodeId]) {
						NODES[dstNodeId]->parent = node;
						nodeDLL = (NODE_DLL*) malloc(sizeof(nodeDLL));
						nodeDLL->node = NODES[dstNodeId];
						nodeDLL->next = NULL;
						nodeDLL->prev = Q_TAIL;
						if (Q_TAIL != NULL) {
							Q_TAIL->next = nodeDLL;
						}
						Q_TAIL = nodeDLL;
						if (Q_HEAD == NULL) {
							Q_HEAD = nodeDLL;
						}
						IN_Q[dstNodeId] = TRUE;
					}
				}
			}
		}
		free(IN_Q);

		/* Finalize all nodes with flow requirements == cap */
		flowLL = FLOW_BUCKET[cap - 1];
		while (flowLL != NULL) {
			flow = flowLL->flow;
			if (flow->srcNodeId == SRC && flow->dstNodeId != SRC) {
				dstNodeId = flow->dstNodeId;
				parent = NODES[dstNodeId]->parent;
				while (parent != NULL) {
					pathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
					pathNode->nodeId = parent->id;
					pathNode->next = NULL;
					PATH[SRC][dstNodeId].distance++;
					if (PATH[SRC][dstNodeId].start == NULL) {
						PATH[SRC][dstNodeId].start = pathNode;
						PATH[SRC][dstNodeId].end = pathNode;
					}
					else {
						PATH[SRC][dstNodeId].end->next = pathNode;
						PATH[SRC][dstNodeId].end = pathNode;
					}
					parent = parent->parent;
				}
			}
			flowLL = flowLL->next;
		}
	}
}





/******************************************************************************/

PATH_CHAIN* getPath (PSEUDO_EDGE* pseudoEdge) {
	PATH_CHAIN* pathChain;
	PATH_CHAIN* pathChainOne;
	PATH_CHAIN* pathChainTwo;
	PATH_NODE* srcPathNode;
	PATH_NODE* dstPathNode;
	
	pathChain = (PATH_CHAIN*) malloc(sizeof(PATH_CHAIN));
	pathChain->first = NULL;
	pathChain->last = NULL;

	if ((pseudoEdge->edgeOne == NULL) && (pseudoEdge->edgeTwo == NULL)) {
		srcPathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
		srcPathNode->nodeId = pseudoEdge->srcNodeId;
		dstPathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
		dstPathNode->nodeId = pseudoEdge->dstNodeId;
		srcPathNode->next = dstPathNode;
		dstPathNode->next = NULL;
		pathChain->first = srcPathNode;
		pathChain->last = dstPathNode;
	}
	else {
		pathChainOne = getPath(pseudoEdge->edgeOne);
		pathChainTwo = getPath(pseudoEdge->edgeTwo);
		pathChainOne->last->next = pathChainTwo->first;
		pathChain->first = pathChainOne->first;
		pathChain->last = pathChainTwo->last;
		free(pathChainOne);
		free(pathChainTwo);
	}
	return pathChain;
}

void shinn_algorithm(PATH** PATH) {

	int i, j;
	unsigned int cap;
	EDGE_LL* edgeLL;
	PSEUDO_EDGE_DLL** F;
	PSEUDO_EDGE_DLL* pseudoEdgeDLL;
	unsigned int curMinDist;
	EDGE* edge;
	unsigned int srcNodeId, dstNodeId;
	unsigned int distance;
	unsigned char add;
	FLOW_LL* flowLL;
	FLOW* flow;
	PATH_CHAIN* pathChain;
	PSEUDO_EDGE*** S;
	PSEUDO_EDGE** tmpPseudoEdgePtrPtr;
	PSEUDO_EDGE* pseudoEdge;
	PSEUDO_EDGE* newPseudoEdge;
	
	/* The solution set is a 2D array of pointers to real/pseudo edges */
	S = (PSEUDO_EDGE***) malloc(NUM_NODES * sizeof(PSEUDO_EDGE**));
	tmpPseudoEdgePtrPtr = (PSEUDO_EDGE**) malloc(NUM_NODES * NUM_NODES * sizeof(PSEUDO_EDGE*));
	for (i = 0; i < NUM_NODES; i++) {
		S[i] = tmpPseudoEdgePtrPtr + (i * NUM_NODES);
	}
	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NUM_NODES; j++) {
			S[i][j] = NULL;
		}
	}

	/* The frontier set is an array of buckets */
	F = (PSEUDO_EDGE_DLL**) malloc(NUM_NODES * sizeof(PSEUDO_EDGE_DLL*));
	for (i = 0; i < NUM_NODES; i++) {
		F[i] = NULL;
	}
	
	for (cap = MAX_CAP; cap > 0; cap--) {
		edgeLL = EDGE_BUCKET[cap - 1];
		while (edgeLL != NULL) {
			edge = edgeLL->edge; /* (u -> v) */
			srcNodeId = edge->srcNodeId;
			dstNodeId = edge->dstNodeId;
			pseudoEdge = S[srcNodeId][dstNodeId];
			if (pseudoEdge == NULL) {
				pseudoEdge = (PSEUDO_EDGE*) malloc(sizeof(PSEUDO_EDGE));
				S[srcNodeId][dstNodeId] = pseudoEdge;
			}
			pseudoEdge->distance = 1;
			pseudoEdge->capacity = cap;
			pseudoEdge->srcNodeId = srcNodeId;
			pseudoEdge->dstNodeId = dstNodeId;
			pseudoEdge->edgeOne = NULL;
			pseudoEdge->edgeTwo = NULL;
			pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
			pseudoEdgeDLL->pseudoEdge = pseudoEdge;
			pseudoEdgeDLL->prev = NULL;
			pseudoEdgeDLL->next = F[1];
			F[1] = pseudoEdgeDLL;
			if (pseudoEdgeDLL->next != NULL) {
				pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
			}
			pseudoEdge->DLL = pseudoEdgeDLL;

			/*
			for (i = 0; i < NUM_NODES; i++) {
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[i][srcNodeId] != NULL)) {
					add = FALSE;
					distance = S[i][srcNodeId]->distance + 1;
					newPseudoEdge = S[i][dstNodeId];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (PSEUDO_EDGE*) malloc(sizeof(PSEUDO_EDGE));
						newPseudoEdge->srcNodeId = i;
						newPseudoEdge->dstNodeId = dstNodeId;
						S[i][dstNodeId] = newPseudoEdge;
						add = TRUE;
					}
					else if (newPseudoEdge->distance > distance) {
						add = TRUE;
					}
					if (add) {
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = S[i][srcNodeId];
						newPseudoEdge->edgeTwo = pseudoEdge;
						pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
				}
			}
			for (i = 0; i < NUM_NODES; i++) {
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[dstNodeId][i] != NULL)) {
					add = FALSE;
					distance = S[dstNodeId][i]->distance + 1;
					newPseudoEdge = S[srcNodeId][i];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (PSEUDO_EDGE*) malloc(sizeof(PSEUDO_EDGE));
						newPseudoEdge->srcNodeId = srcNodeId;
						newPseudoEdge->dstNodeId = i;
						S[srcNodeId][i] = newPseudoEdge;
						add = TRUE;
					}
					else if (newPseudoEdge->distance > distance) {
						add = TRUE;
					}
					if (add) {
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = pseudoEdge;
						newPseudoEdge->edgeTwo = S[dstNodeId][i];
						pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
				}
			}
			*/
			edgeLL = edgeLL->next;
		}

		/* Process pseudo edges until the Frontier set is empty */
		curMinDist = 1;
		while (F[curMinDist] != NULL) {
			pseudoEdgeDLL = F[curMinDist];
			F[curMinDist] = pseudoEdgeDLL->next;
			if (F[curMinDist] != NULL) {
				F[curMinDist]->prev = NULL;
			}
			pseudoEdge = pseudoEdgeDLL->pseudoEdge; /* u -> v */
			pseudoEdge->DLL = NULL;
			srcNodeId = pseudoEdge->srcNodeId;
			dstNodeId = pseudoEdge->dstNodeId;
			free(pseudoEdgeDLL);
			for (i = 0; i < NUM_NODES; i++) {
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[i][srcNodeId] != NULL) && (S[i][srcNodeId]->distance == 1)) {
					distance = pseudoEdge->distance + 1;
					newPseudoEdge = S[i][dstNodeId];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (PSEUDO_EDGE*) malloc(sizeof(PSEUDO_EDGE));
						newPseudoEdge->srcNodeId = i;
						newPseudoEdge->dstNodeId = dstNodeId;
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = S[i][srcNodeId];
						newPseudoEdge->edgeTwo = pseudoEdge;
						S[i][dstNodeId] = newPseudoEdge;
						pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
					else if (newPseudoEdge->distance > distance) {
						newPseudoEdge->edgeOne = S[i][srcNodeId];
						newPseudoEdge->edgeTwo = pseudoEdge;
						pseudoEdgeDLL = newPseudoEdge->DLL;
						if (pseudoEdgeDLL == NULL) {
							pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
							pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
							newPseudoEdge->DLL = pseudoEdgeDLL;
						}
						else {
							if (pseudoEdgeDLL->prev != NULL) {
								pseudoEdgeDLL->prev->next = pseudoEdgeDLL->next;
							}
							else {
								F[newPseudoEdge->distance] = pseudoEdgeDLL->next;
							}
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL->prev;
							}
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
						}
						newPseudoEdge->distance = distance;
					}
				}
			}
			for (i = 0; i < NUM_NODES; i++) { /* For all u -> v -> i where v -> i is a real edge */
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[dstNodeId][i] != NULL) && (S[dstNodeId][i]->distance == 1)) {
					distance = pseudoEdge->distance + 1;
					newPseudoEdge = S[srcNodeId][i];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (PSEUDO_EDGE*) malloc(sizeof(PSEUDO_EDGE));
						newPseudoEdge->srcNodeId = srcNodeId;
						newPseudoEdge->dstNodeId = i;
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = pseudoEdge;
						newPseudoEdge->edgeTwo = S[dstNodeId][i];
						S[srcNodeId][i] = newPseudoEdge;
						pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
					else if (newPseudoEdge->distance > distance) {
						newPseudoEdge->edgeOne = pseudoEdge;
						newPseudoEdge->edgeTwo = S[dstNodeId][i];
						pseudoEdgeDLL = newPseudoEdge->DLL;
						if (pseudoEdgeDLL == NULL) { /* add to F */
							pseudoEdgeDLL = (PSEUDO_EDGE_DLL*) malloc(sizeof(PSEUDO_EDGE_DLL));
							pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
							newPseudoEdge->DLL = pseudoEdgeDLL;
						}
						else {
							if (pseudoEdgeDLL->prev != NULL) {
								pseudoEdgeDLL->prev->next = pseudoEdgeDLL->next;
							}
							else {
								F[newPseudoEdge->distance] = pseudoEdgeDLL->next;
							}
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL->prev;
							}
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
						}
						newPseudoEdge->distance = distance;
					}
				}
			}

			/* Search for the next non-empty bucket */
			while (curMinDist < NUM_NODES - 1) {
				if (F[curMinDist] == NULL) {
					curMinDist++;
				}
				else {
					break;
				}
			}
		}

		/* Finalize all nodes with flow requirements == cap */
		flowLL = FLOW_BUCKET[cap - 1];
		while (flowLL != NULL) {
			flow = flowLL->flow;
			srcNodeId = flow->srcNodeId;
			dstNodeId = flow->dstNodeId;
			if (S[srcNodeId][dstNodeId] != NULL) {
				pathChain = getPath(S[srcNodeId][dstNodeId]);
				PATH[srcNodeId][dstNodeId].distance = S[srcNodeId][dstNodeId]->distance;
				PATH[srcNodeId][dstNodeId].start = pathChain->first;
				PATH[srcNodeId][dstNodeId].end = pathChain->last;
				free(pathChain);
			}
			flowLL = flowLL->next;
		}
	}

	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NUM_NODES; j++) {
			free(S[i][j]);
		}
	}
	free(S);
	free(F);
}





/******************************************************************************/

int main(int argc, char** argv) {

	int i, j;
	unsigned int runNum;
	unsigned int seed;
	unsigned int numEdges, numFlows;
	EDGE** tmpEdgePtrPtr;
	FLOW** tmpFlowPtrPtr;
	PATH** PATH1;
	PATH** PATH2;
	PATH* tmpPathPtr;
	clock_t startTime, endTime;
	double timeSimple, timeShinn;
	PATH_NODE* pathNode;
	PATH_NODE* nextPathNode;
	EDGE_LL* edgeLL;
	FLOW_LL* flowLL;
	
	if (argc != 5) {
		printf("Usage: %s <NUM_NODES> <MULTIPLIER> <MAX_CAP> <NUM_RUNS>\n", argv[0]);
		return 0;
	}

	NUM_NODES = atoi(argv[1]);
	NUM_EDGES = atoi(argv[2]) * NUM_NODES;
	if (NUM_EDGES > ((NUM_NODES * NUM_NODES) - NUM_NODES)) {
		NUM_EDGES = (NUM_NODES * NUM_NODES) - NUM_NODES;
	}
	MAX_CAP   = atoi(argv[3]);
	NUM_RUNS  = atoi(argv[4]);
	seed = time(NULL);
	/* seed = 1361845750; */
	srand(seed);
	printf("\nNUM_NODES = %d, NUM_EDGES = %d, MAX_CAP = %d, NUM_RUNS = %d, seed = %d\n", NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS, seed);

	/* Initialize 2D arrays for storing Edge capacities and Flow requirements and current distances */
	E = (EDGE***) malloc(NUM_NODES * sizeof(EDGE**));
	tmpEdgePtrPtr = (EDGE**) malloc(NUM_NODES * NUM_NODES * sizeof(EDGE*));
	for (i = 0; i < NUM_NODES; i++) {
		E[i] = tmpEdgePtrPtr + (i * NUM_NODES);
	}
	F = (FLOW***) malloc(NUM_NODES * sizeof(FLOW**));
	tmpFlowPtrPtr = (FLOW**) malloc(NUM_NODES * NUM_NODES * sizeof(FLOW*));
	for (i = 0; i < NUM_NODES; i++) {
		F[i] = tmpFlowPtrPtr + (i * NUM_NODES);
	}
	
	/* Initialize 1D array of pointers to nodes */
	NODES = (NODE**) malloc(NUM_NODES * sizeof(NODE*));
	for (i = 0; i < NUM_NODES; i++) {
		NODES[i] = (NODE*) malloc(sizeof(NODE));
		NODES[i]->id = i;
	}
	
	/* Initialize bucket system for sorting flows and edges by their capacities */
	FLOW_BUCKET = (FLOW_LL**) malloc(MAX_CAP * sizeof(FLOW_LL*));
	EDGE_BUCKET = (EDGE_LL**) malloc(MAX_CAP * sizeof(EDGE_LL*));
	for (i = 0; i < MAX_CAP; i++) {
		FLOW_BUCKET[i] = NULL;
		EDGE_BUCKET[i] = NULL;
	}

	/* Initialize the structure for storing the solution paths for all flows */
	PATH1 = (PATH**) malloc(NUM_NODES * sizeof(PATH*));
	tmpPathPtr = (PATH*) malloc(NUM_NODES * NUM_NODES * sizeof(PATH));
	for (i = 0; i < NUM_NODES; i++) {
		PATH1[i] = tmpPathPtr + (i * NUM_NODES);
	}
	PATH2 = (PATH**) malloc(NUM_NODES * sizeof(PATH*));
	tmpPathPtr = (PATH*) malloc(NUM_NODES * NUM_NODES * sizeof(PATH));
	for (i = 0; i < NUM_NODES; i++) {
		PATH2[i] = tmpPathPtr + (i * NUM_NODES);
	}


	
	/*********************
	 ***** MAIN LOOP *****
	 *********************/
	for (runNum = 0; runNum < NUM_RUNS; runNum++) {

		printf("\n================ RUN #%d ================\n", runNum);

		/* Initialize structures and generate random capacities/bandwidths */
		for (i = 0; i < NUM_NODES; i++) {
			NODES[i]->parent = NULL;
			NODES[i]->numToEdges = 0;
			NODES[i]->numFrEdges = 0;
			NODES[i]->toEdges = NULL;
			NODES[i]->frEdges = NULL;
			for (j = 0; j < NUM_NODES; j++) {
				E[i][j] = NULL;
				F[i][j] = NULL;
			}
		}
		numFlows = 0;
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (i != j) {
					numFlows++;
					F[i][j] = (FLOW*) malloc(sizeof(FLOW));
					F[i][j]->srcNodeId = i;
					F[i][j]->dstNodeId = j;
					F[i][j]->bandwidth = (unsigned int) rand() % MAX_CAP + 1;
				}
			}
		}
		numEdges = 0;
		while (numEdges < NUM_EDGES) {
			i = rand() % NUM_NODES;
			j = rand() % NUM_NODES;
			if (i != j) {
				if (E[i][j] == NULL) {
					numEdges++;
					E[i][j] = (EDGE*) malloc(sizeof(EDGE));
					E[i][j]->srcNodeId = i;
					E[i][j]->dstNodeId = j;
					E[i][j]->capacity = (unsigned int) rand() % MAX_CAP + 1;
				}
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				PATH1[i][j].distance = 0;
				PATH1[i][j].start = NULL;
				PATH1[i][j].end = NULL;
				PATH2[i][j].distance = 0;
				PATH2[i][j].start = NULL;
				PATH2[i][j].end = NULL;
				if (E[i][j] != NULL) {
					edgeLL = (EDGE_LL*) malloc(sizeof(EDGE_LL));
					edgeLL->edge = E[i][j];
					edgeLL->next = EDGE_BUCKET[E[i][j]->capacity - 1];
					EDGE_BUCKET[E[i][j]->capacity - 1] = edgeLL;
					NODES[i]->numToEdges++;
					NODES[i]->toEdges = realloc(NODES[i]->toEdges, NODES[i]->numToEdges * sizeof(EDGE*));
					NODES[i]->toEdges[NODES[i]->numToEdges - 1] = E[i][j];
					NODES[j]->numFrEdges++;
					NODES[j]->frEdges = realloc(NODES[j]->frEdges, NODES[j]->numFrEdges * sizeof(EDGE*));
					NODES[j]->frEdges[NODES[j]->numFrEdges - 1] = E[i][j];
				}
				if (F[i][j] != NULL) {
					flowLL = (FLOW_LL*) malloc(sizeof(FLOW_LL));
					flowLL->flow = F[i][j];
					flowLL->next = FLOW_BUCKET[F[i][j]->bandwidth - 1];
					FLOW_BUCKET[F[i][j]->bandwidth - 1] = flowLL;
				}
			}
		}

		startTime = clock();
		for (i = 0; i < NUM_NODES; i++) {
			simple_algorithm(i, PATH1);
			for (j = 0; j < NUM_NODES; j++) {
				NODES[j]->parent = NULL;
			}
		}
		endTime = clock();
		timeSimple = (double) (endTime - startTime) / CLOCKS_PER_SEC;

		startTime = clock();
		shinn_algorithm(PATH2);
		endTime = clock();
		timeShinn = (double) (endTime - startTime) / CLOCKS_PER_SEC;

		printf("Simple Algorithm  : %.2f seconds\n", timeSimple);
		printf("Shinn's Algorithm : %.2f seconds\n", timeShinn);

		/* Check results */
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (i != j) {
					pathNode = PATH2[i][j].start;
					while (pathNode != NULL) {
						nextPathNode = pathNode->next;
						if (nextPathNode == NULL) {
							break;
						}
						if (pathNode->nodeId != nextPathNode->nodeId) {
							if (E[pathNode->nodeId][nextPathNode->nodeId]->capacity < F[i][j]->bandwidth) {
								printf("!!! %d -> %d ERROR !!!", pathNode->nodeId, nextPathNode->nodeId);
							}
						}
						pathNode = nextPathNode;
					}
				}
			}

		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (PATH1[i][j].distance != PATH2[i][j].distance) {
					printf("\n\n !!!ALGORITHM IS BROKEN!!! \n\n");
				}
			}
		}

		/* Clean up for the next run */
		for (i = 0; i < NUM_NODES; i++) {
			free(NODES[i]->toEdges);
			free(NODES[i]->frEdges);
			for (j = 0; j < NUM_NODES; j++) {
				free(E[i][j]);
				free(F[i][j]);
			}
		}
		for (i = 0; i < MAX_CAP; i++) {
			while (EDGE_BUCKET[i] != NULL) {
				edgeLL = EDGE_BUCKET[i];
				EDGE_BUCKET[i] = edgeLL->next;
				free(edgeLL);
			}
			while (FLOW_BUCKET[i] != NULL) {
				flowLL = FLOW_BUCKET[i];
				FLOW_BUCKET[i] = flowLL->next;
				free(flowLL);
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				while (PATH1[i][j].start != NULL) {
					pathNode = PATH1[i][j].start;
					PATH1[i][j].start = pathNode->next;
					free(pathNode);
				}
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				while (PATH2[i][j].start != NULL) {
					pathNode = PATH2[i][j].start;
					PATH2[i][j].start = pathNode->next;
					free(pathNode);
				}
			}
		}
	}

	return 0;
}

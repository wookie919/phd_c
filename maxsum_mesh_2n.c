#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

#define TRUE 1
#define FALSE 0

#define DEBUG TRUE


int serial(int** A, int N) {

	int i, j, k;
	int NN;
	int SUM = -2 * N;
	int *tmpIntPtr;
	int **minSumA, **recSumA, **solSumA, **colSumA;
	int maxSum;

	NN = N * N;

	minSumA = (int**) malloc(N * sizeof(int*));
	recSumA = (int**) malloc(N * sizeof(int*));
	solSumA = (int**) malloc(N * sizeof(int*));
	colSumA = (int**) malloc(N * sizeof(int*));
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		minSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		recSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		solSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		colSumA[i] = tmpIntPtr + (i * N);
	}
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			minSumA[i][j] = 0;
			recSumA[i][j] = 0;
			solSumA[i][j] = (-2 * N);
			colSumA[i][j] = 0;
		}
	}

	for (k = 0; k < N; k++) {
		if (k > 0) {
			for (j = 0; j < N; j++) {
				colSumA[k - 1][j] = 0;
			}
		}
		for (i = k; i < N; i++) {
			for (j = 0; j < N; j++) {
				if (i == 0) {
					colSumA[i][j] = A[i][j];
				}
				else {
					colSumA[i][j] = colSumA[i - 1][j] + A[i][j];
				}
				if (j == 0) {
					recSumA[i][j] = colSumA[i][j];
				}
				else {
					recSumA[i][j] = recSumA[i][j - 1] + colSumA[i][j];
				}
				if (j == 0) {
					if (recSumA[i][j] < 0) {
						minSumA[i][j] = recSumA[i][j];
					}
					else {
						minSumA[i][j] = 0;
					}
				}
				else {
					if (recSumA[i][j] < minSumA[i][j - 1]) {
						minSumA[i][j] = recSumA[i][j];
					}
					else {
						minSumA[i][j] = minSumA[i][j - 1];
					}
				}
				maxSum = recSumA[i][j] - minSumA[i][j];
				if (j == 0){
					solSumA[i][j] = maxSum;
				}
				else {
					if (maxSum > solSumA[i][j - 1]) {
						solSumA[i][j] = maxSum;
					}
					else {
						solSumA[i][j] = solSumA[i][j - 1];
					}
				}
			}
			if (solSumA[i][N - 1] > SUM) {
				SUM = solSumA[i][N - 1];
			}
		}
	}

	return SUM;
}


int main(int argc, char ** argv) {

	int i, j, k;
	int N, NN;
	int SUM_SERIAL;
	int SUM_PARALLEL;

	int SIZE, MASTER;
	int RANK, ROW, COL;
	int RANK_T, RANK_B, RANK_L, RANK_R;

	char TOP_EDGE = FALSE;
	char BOTTOM_EDGE = FALSE;
	char LEFT_EDGE = FALSE;
	char RIGHT_EDGE = FALSE;
	char LHS = FALSE;
	char RHS = FALSE;

	int *tmpIntPtr; /* Used for memory allocation of matrices */
	int **A; /* 2D array */

	int seed;
	int cellRank;
	int A_VAL;

	int colSum = 0;
	int solSum = 0;
	int recSum = 0;
	int minSum = 0;
	int midSum = 0;
	int maxSum = 0;

	int colSumFromT = 0, solSumFromT = 0, midSumFromT = 0;
	int recSumFromL = 0, minSumFromL = 0, solSumFromL = 0;
	int recSumFromR = 0, minSumFromR = 0, solSumFromR = 0, maxSumFromR = 0;

	int step;

	int bottomMidRankL, bottomMidRankR;

	int maxSumCandidate1, maxSumCandidate2, maxSumCandidate3;
	MPI_Status status;

	clock_t startTime, endTime;
	double timeTaken;

	/* Check parameters and initialize all processes */
	/*
	if (argc != 2) {
		printf("Usage: %s <n>\n", argv[0]);
		return 0;
	}
	N = atoi(argv[1]);
	*/
	
	N = 8;
	
	NN = N * N;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	if ((NN + 1) != SIZE) {
		if (RANK == MASTER) {
			printf("Number of processors must be n^2 + 1\n");
		}
		return 0;
	}
	MASTER = SIZE - 1;

	if (RANK == MASTER) {
		/***********************************************************************
		 *
		 * Code for MASTER
		 *
		 **********************************************************************/
		seed = time(NULL);
		//seed = 1400047468;
		srand(seed);
	
		/* Initialize the 2D array */
		A = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NN * sizeof(int));
		for (i = 0; i < N; i++) {
			A[i] = tmpIntPtr + (i * N);
		}
		
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				A[i][j] = (rand() % (2 * N)) - N;
			}
		}
		
		if (DEBUG) {
			printf("======== A ========\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", A[i][j]);
				}
				printf("\n");
			}
		}
		
		/* Transmit each A[i][j] to each cell */
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				cellRank = (i * N) + j;
				MPI_Send(&(A[i][j]), 1, MPI_INT, cellRank, 0, MPI_COMM_WORLD);
			}
		}
		
		/* All cells are now initialized */
		MPI_Barrier(MPI_COMM_WORLD);
		
		/* Main computation loop */
		startTime = clock();
		for (step = 0; step < 2 * N; step++) {
			if (DEBUG) {
				printf("==== step: %d ====\n", step);
			}
			MPI_Barrier(MPI_COMM_WORLD); /* Sync after each step just because. */
		}
		endTime = clock();
		timeTaken = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		printf("N = %d, seed = %d, time taken : %.4f seconds\n", N, seed, timeTaken);

		/* Receive the maximum sum from CELL[N - 1][N - 1] */
		MPI_Recv(&SUM_PARALLEL, 1, MPI_INT, MASTER - 1, 0, MPI_COMM_WORLD, &status);
		
		if (DEBUG) {
			SUM_SERIAL = serial(A, N);
			if (SUM_SERIAL != SUM_PARALLEL) {
				printf("FAILED: %d != %d\n", SUM_SERIAL, SUM_PARALLEL);
			}
			else {
				printf("PASSED: %d == %d\n", SUM_SERIAL, SUM_PARALLEL);
			}
		}
	}
	
	/***************************************************************************
	 *
	 * Code for Cells
	 *
	 **************************************************************************/
	else {
		ROW = RANK / N;
		COL = RANK % N;

		/* Determine cell rank of neighbour cells */
		RANK_T = (ROW - 1) * N + COL;
		RANK_B = (ROW + 1) * N + COL;
		RANK_L = RANK - 1;
		RANK_R = RANK + 1;

		/* Determine edge cells */
		if (ROW == 0) {
			TOP_EDGE = TRUE;
		}
		else if (ROW == N - 1) {
			BOTTOM_EDGE = TRUE;
		}
		if (COL == 0) {
			LEFT_EDGE = TRUE;
		}
		else if (COL == N - 1) {
			RIGHT_EDGE = TRUE;
		}

		/* Divide the cells into LHS and RHS */
		if (COL < N / 2) {
			LHS = TRUE;
		}
		else {
			RHS = TRUE;
		}

		/* Receive the value of A[ROW][COL] */
		MPI_Recv(&A_VAL, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD, &status);
		MPI_Barrier(MPI_COMM_WORLD);

		/******** MAIN LOOP ********/
		for (step = 0; step < 2 * N; step++) {

			/* Send colSum and solSum to RANK_B */
			if (TOP_EDGE) {
				MPI_Send(&colSum, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD);
				MPI_Send(&solSum, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD);
			}
			else if (BOTTOM_EDGE) {
				MPI_Recv(&colSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&solSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&colSum, 1, MPI_INT, RANK_B, step,
					&colSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_B, step,
					&solSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
			}
			
			/* Send recSum, minSum, solSum to RANK_R */
			if (LEFT_EDGE) {
				MPI_Send(&recSum, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				MPI_Send(&minSum, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				MPI_Send(&solSum, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
			}
			else if (RIGHT_EDGE) {
				MPI_Recv(&recSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&minSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&solSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&recSum, 1, MPI_INT, RANK_R, step,
					&recSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&minSum, 1, MPI_INT, RANK_R, step,
					&minSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_R, step,
					&solSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
			}

			/* Main computation */
			if (COL <= step) {
				colSum = colSumFromT + A_VAL;
				recSum = recSumFromL + colSum;
				if (recSum < minSumFromL) {
					minSum = recSum;
				}
				else {
					minSum = minSumFromL;
				}
				maxSum = recSum - minSum;
				if (solSum < maxSum) {
					solSum = maxSum;
				}
				if (solSum < solSumFromT) {
					solSum = solSumFromT;
				}
				if (solSum < solSumFromL) {
					solSum = solSumFromL;
				}
			}

			MPI_Barrier(MPI_COMM_WORLD); /* Sync each step */
		}
		
		if ((ROW == N - 1) && (COL == N - 1)) {
			MPI_Send(&solSum, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD);
		}
	}
	
	MPI_Finalize();
	
	return 0;
}



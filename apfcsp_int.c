#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

typedef struct _NODE {
	unsigned int id;
	struct _EDGE_LLN* edges;
	struct _NODE* parent;
} NODE;

typedef struct _NODE_DLLN {
	NODE* node;
	struct _NODE_DLLN* prev;
	struct _NODE_DLLN* next;
} NODE_DLLN;

typedef struct _EDGE {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int capacity;
} EDGE;

typedef struct _EDGE_LLN {
	EDGE* edge;
	struct _EDGE_LLN* next;
} EDGE_LLN;

typedef struct _FLOW {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int bandwidth;
} FLOW;

typedef struct _FLOW_LLN {
	FLOW* flow;
	struct _FLOW_LLN* next;
} FLOW_LLN;

typedef struct _P_EDGE {
	unsigned int srcNodeId;
	unsigned int dstNodeId;
	unsigned int capacity;
	unsigned int distance;
	struct _P_EDGE* edgeOne;
	struct _P_EDGE* edgeTwo;
	struct _P_EDGE_DLLN* DLL;
} P_EDGE;

typedef struct _P_EDGE_DLLN {
	struct _P_EDGE* pseudoEdge;
	struct _P_EDGE_DLLN* prev;
	struct _P_EDGE_DLLN* next;
} P_EDGE_DLLN;

typedef struct _PATH_NODE {
	unsigned int nodeId;
	struct _PATH_NODE* next;
} PATH_NODE;

typedef struct _PATH {
	unsigned int distance;
	struct _PATH_NODE* start;
	struct _PATH_NODE* end;
} PATH;

typedef struct _PATH_CHAIN {
	struct _PATH_NODE* first;
	struct _PATH_NODE* last;
} PATH_CHAIN;

static unsigned int NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS;
static EDGE*** EDGES;
static FLOW*** FLOWS;
static EDGE_LLN** EDGE_BUCKET;
static FLOW_LLN** FLOW_BUCKET;





/******************************************************************************/

void simple_algorithm(PATH** PATH) {

	int i, j;
	unsigned int src;
	unsigned int cap;
	unsigned int dstNodeId;
	unsigned char* IN_Q;
	NODE* node;
	NODE* parent;
	NODE** NODES;
	NODE_DLLN* nodeDLL;
	NODE_DLLN* Q_HEAD;
	NODE_DLLN* Q_TAIL;
	FLOW* flow;
	FLOW_LLN* flowLL;
	EDGE* edge;
	EDGE_LLN* edgeLL;
	PATH_NODE* pathNode;

	NODES = (NODE**) malloc(NUM_NODES * sizeof(NODE*));
	for (i = 0; i < NUM_NODES; i++) {
		NODES[i] = (NODE*) malloc(sizeof(NODE));
		NODES[i]->id = i;
		NODES[i]->edges = NULL;
		NODES[i]->parent = NULL;
	}

	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NUM_NODES; j++) {
			if (EDGES[i][j] != NULL) {
				edgeLL = (EDGE_LLN*) malloc(sizeof(EDGE_LLN));
				edgeLL->edge = EDGES[i][j];
				edgeLL->next = NODES[i]->edges;
				NODES[i]->edges = edgeLL;
			}
		}
	}

	IN_Q = (unsigned char*) malloc(NUM_NODES * sizeof(unsigned char));
	for (i = 0; i < NUM_NODES; i++) {
		IN_Q[i] = FALSE;
	}
	
	for (src = 0; src < NUM_NODES; src++) {
		for (cap = MAX_CAP; cap > 0; cap--) {

			IN_Q[src] = TRUE;
			nodeDLL = (NODE_DLLN*) malloc(sizeof(nodeDLL));
			nodeDLL->node = NODES[src];
			nodeDLL->next = NULL;
			nodeDLL->prev = NULL;
			Q_HEAD = nodeDLL;
			Q_TAIL = nodeDLL;

			while (Q_HEAD != NULL) {
				nodeDLL = Q_HEAD;
				Q_HEAD = nodeDLL->next;
				if (Q_HEAD == NULL) {
					Q_TAIL = NULL;
				}
				else {
					Q_HEAD->prev = NULL;
				}
				nodeDLL->next = NULL;
				nodeDLL->prev = NULL;
				node = nodeDLL->node;
				free(nodeDLL);
				edgeLL = node->edges;
				while (edgeLL != NULL) {
					edge = edgeLL->edge;
					dstNodeId = edge->dstNodeId;
					if (edge->capacity >= cap) {
						if (!IN_Q[dstNodeId]) {
							NODES[dstNodeId]->parent = node;
							nodeDLL = (NODE_DLLN*) malloc(sizeof(nodeDLL));
							nodeDLL->node = NODES[dstNodeId];
							nodeDLL->next = NULL;
							nodeDLL->prev = Q_TAIL;
							if (Q_TAIL != NULL) {
								Q_TAIL->next = nodeDLL;
							}
							Q_TAIL = nodeDLL;
							if (Q_HEAD == NULL) {
								Q_HEAD = nodeDLL;
							}
							IN_Q[dstNodeId] = TRUE;
						}
					}
					edgeLL = edgeLL->next;
				}
			}

			flowLL = FLOW_BUCKET[cap];
			while (flowLL != NULL) {
				flow = flowLL->flow;
				if (flow->srcNodeId == src && flow->dstNodeId != src) {
					dstNodeId = flow->dstNodeId;
					parent = NODES[dstNodeId]->parent;
					while (parent != NULL) {
						pathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
						pathNode->nodeId = parent->id;
						pathNode->next = NULL;
						PATH[src][dstNodeId].distance++;
						if (PATH[src][dstNodeId].start == NULL) {
							PATH[src][dstNodeId].start = pathNode;
							PATH[src][dstNodeId].end = pathNode;
						}
						else {
							PATH[src][dstNodeId].end->next = pathNode;
							PATH[src][dstNodeId].end = pathNode;
						}
						parent = parent->parent;
					}
				}
				flowLL = flowLL->next;
			}

			for (i = 0; i < NUM_NODES; i++) {
				NODES[i]->parent = NULL;
			}
			for (i = 0; i < NUM_NODES; i++) {
				IN_Q[i] = FALSE;
			}
		}
	}
	
	for (i = 0; i < NUM_NODES; i++) {
		edgeLL = NODES[i]->edges;
		while (edgeLL != NULL) {
			NODES[i]->edges = edgeLL->next;
			free(edgeLL);
			edgeLL = NODES[i]->edges;
		}
	}
	free(NODES);
	free(IN_Q);
}





/******************************************************************************/

PATH_CHAIN* getPath (P_EDGE* pseudoEdge) {
	PATH_CHAIN* pathChain;
	PATH_CHAIN* pathChainOne;
	PATH_CHAIN* pathChainTwo;
	PATH_NODE* srcPathNode;
	PATH_NODE* dstPathNode;

	pathChain = (PATH_CHAIN*) malloc(sizeof(PATH_CHAIN));
	pathChain->first = NULL;
	pathChain->last = NULL;

	if ((pseudoEdge->edgeOne == NULL) && (pseudoEdge->edgeTwo == NULL)) {
		srcPathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
		srcPathNode->nodeId = pseudoEdge->srcNodeId;
		dstPathNode = (PATH_NODE*) malloc(sizeof(PATH_NODE));
		dstPathNode->nodeId = pseudoEdge->dstNodeId;
		srcPathNode->next = dstPathNode;
		dstPathNode->next = NULL;
		pathChain->first = srcPathNode;
		pathChain->last = dstPathNode;
	}
	else {
		pathChainOne = getPath(pseudoEdge->edgeOne);
		pathChainTwo = getPath(pseudoEdge->edgeTwo);
		pathChainOne->last->next = pathChainTwo->first;
		pathChain->first = pathChainOne->first;
		pathChain->last = pathChainTwo->last;
		free(pathChainOne);
		free(pathChainTwo);
	}
	return pathChain;
}


void shinn_algorithm(PATH** PATH) {

	int i, j;
	unsigned int cap;
	unsigned int curMinDist;
	unsigned int srcNodeId, dstNodeId;
	unsigned int distance;
	EDGE* edge;
	EDGE_LLN* edgeLL;
	P_EDGE_DLLN** F;
	P_EDGE_DLLN* pseudoEdgeDLL;
	FLOW* flow;
	FLOW_LLN* flowLL;
	PATH_CHAIN* pathChain;
	P_EDGE*** S;
	P_EDGE** tmpPseudoEdgePtrPtr;
	P_EDGE* pseudoEdge;
	P_EDGE* newPseudoEdge;

	S = (P_EDGE***) malloc(NUM_NODES * sizeof(P_EDGE**));
	tmpPseudoEdgePtrPtr = (P_EDGE**) malloc(NUM_NODES * NUM_NODES * sizeof(P_EDGE*));
	for (i = 0; i < NUM_NODES; i++) {
		S[i] = tmpPseudoEdgePtrPtr + (i * NUM_NODES);
	}
	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NUM_NODES; j++) {
			S[i][j] = NULL;
		}
	}

	F = (P_EDGE_DLLN**) malloc(NUM_NODES * sizeof(P_EDGE_DLLN*));
	for (i = 0; i < NUM_NODES; i++) {
		F[i] = NULL;
	}

	for (cap = MAX_CAP; cap > 0; cap--) {
		edgeLL = EDGE_BUCKET[cap];
		while (edgeLL != NULL) {
			edge = edgeLL->edge;
			srcNodeId = edge->srcNodeId;
			dstNodeId = edge->dstNodeId;
			pseudoEdge = S[srcNodeId][dstNodeId];
			if (pseudoEdge == NULL) {
				pseudoEdge = (P_EDGE*) malloc(sizeof(P_EDGE));
				S[srcNodeId][dstNodeId] = pseudoEdge;
			}
			pseudoEdge->distance = 1;
			pseudoEdge->capacity = cap;
			pseudoEdge->srcNodeId = srcNodeId;
			pseudoEdge->dstNodeId = dstNodeId;
			pseudoEdge->edgeOne = NULL;
			pseudoEdge->edgeTwo = NULL;
			pseudoEdgeDLL = (P_EDGE_DLLN*) malloc(sizeof(P_EDGE_DLLN));
			pseudoEdgeDLL->pseudoEdge = pseudoEdge;
			pseudoEdgeDLL->prev = NULL;
			pseudoEdgeDLL->next = F[1];
			F[1] = pseudoEdgeDLL;
			if (pseudoEdgeDLL->next != NULL) {
				pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
			}
			pseudoEdge->DLL = pseudoEdgeDLL;
			edgeLL = edgeLL->next;
		}

		curMinDist = 1;
		while (F[curMinDist] != NULL) {
			pseudoEdgeDLL = F[curMinDist]; /* <u,v> */
			F[curMinDist] = pseudoEdgeDLL->next;
			if (F[curMinDist] != NULL) {
				F[curMinDist]->prev = NULL;
			}
			pseudoEdge = pseudoEdgeDLL->pseudoEdge;
			pseudoEdge->DLL = NULL;
			srcNodeId = pseudoEdge->srcNodeId;
			dstNodeId = pseudoEdge->dstNodeId;
			free(pseudoEdgeDLL);
			for (i = 0; i < NUM_NODES; i++) { /* (t,u) + <u,v> */
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[i][srcNodeId] != NULL) && (S[i][srcNodeId]->distance == 1)) {
					distance = pseudoEdge->distance + 1;
					newPseudoEdge = S[i][dstNodeId];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (P_EDGE*) malloc(sizeof(P_EDGE));
						newPseudoEdge->srcNodeId = i;
						newPseudoEdge->dstNodeId = dstNodeId;
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = S[i][srcNodeId];
						newPseudoEdge->edgeTwo = pseudoEdge;
						S[i][dstNodeId] = newPseudoEdge;
						pseudoEdgeDLL = (P_EDGE_DLLN*) malloc(sizeof(P_EDGE_DLLN));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
					else if (newPseudoEdge->distance > distance) {
						newPseudoEdge->edgeOne = S[i][srcNodeId];
						newPseudoEdge->edgeTwo = pseudoEdge;
						pseudoEdgeDLL = newPseudoEdge->DLL;
						if (pseudoEdgeDLL == NULL) {
							pseudoEdgeDLL = (P_EDGE_DLLN*) malloc(sizeof(P_EDGE_DLLN));
							pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
							newPseudoEdge->DLL = pseudoEdgeDLL;
						}
						else {
							if (pseudoEdgeDLL->prev != NULL) {
								pseudoEdgeDLL->prev->next = pseudoEdgeDLL->next;
							}
							else {
								F[newPseudoEdge->distance] = pseudoEdgeDLL->next;
							}
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL->prev;
							}
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
						}
						newPseudoEdge->distance = distance;
					}
				}
			}
			for (i = 0; i < NUM_NODES; i++) { /* <u,v> + (v,w) */
				if ((i != srcNodeId) && (i != dstNodeId) &&
					(S[dstNodeId][i] != NULL) && (S[dstNodeId][i]->distance == 1)) {
					distance = pseudoEdge->distance + 1;
					newPseudoEdge = S[srcNodeId][i];
					if (newPseudoEdge == NULL) {
						newPseudoEdge = (P_EDGE*) malloc(sizeof(P_EDGE));
						newPseudoEdge->srcNodeId = srcNodeId;
						newPseudoEdge->dstNodeId = i;
						newPseudoEdge->capacity = cap;
						newPseudoEdge->distance = distance;
						newPseudoEdge->edgeOne = pseudoEdge;
						newPseudoEdge->edgeTwo = S[dstNodeId][i];
						S[srcNodeId][i] = newPseudoEdge;
						pseudoEdgeDLL = (P_EDGE_DLLN*) malloc(sizeof(P_EDGE_DLLN));
						pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
						pseudoEdgeDLL->prev = NULL;
						pseudoEdgeDLL->next = F[distance];
						F[distance] = pseudoEdgeDLL;
						if (pseudoEdgeDLL->next != NULL) {
							pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
						}
						newPseudoEdge->DLL = pseudoEdgeDLL;
					}
					else if (newPseudoEdge->distance > distance) {
						newPseudoEdge->edgeOne = pseudoEdge;
						newPseudoEdge->edgeTwo = S[dstNodeId][i];
						pseudoEdgeDLL = newPseudoEdge->DLL;
						if (pseudoEdgeDLL == NULL) {
							pseudoEdgeDLL = (P_EDGE_DLLN*) malloc(sizeof(P_EDGE_DLLN));
							pseudoEdgeDLL->pseudoEdge = newPseudoEdge;
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
							newPseudoEdge->DLL = pseudoEdgeDLL;
						}
						else {
							if (pseudoEdgeDLL->prev != NULL) {
								pseudoEdgeDLL->prev->next = pseudoEdgeDLL->next;
							}
							else {
								F[newPseudoEdge->distance] = pseudoEdgeDLL->next;
							}
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL->prev;
							}
							pseudoEdgeDLL->prev = NULL;
							pseudoEdgeDLL->next = F[distance];
							F[distance] = pseudoEdgeDLL;
							if (pseudoEdgeDLL->next != NULL) {
								pseudoEdgeDLL->next->prev = pseudoEdgeDLL;
							}
						}
						newPseudoEdge->distance = distance;
					}
				}
			}

			while (curMinDist < NUM_NODES - 1) {
				if (F[curMinDist] == NULL) {
					curMinDist++;
				}
				else {
					break;
				}
			}
		}

		flowLL = FLOW_BUCKET[cap];
		while (flowLL != NULL) {
			flow = flowLL->flow;
			srcNodeId = flow->srcNodeId;
			dstNodeId = flow->dstNodeId;
			if (S[srcNodeId][dstNodeId] != NULL) {
				pathChain = getPath(S[srcNodeId][dstNodeId]);
				PATH[srcNodeId][dstNodeId].distance = S[srcNodeId][dstNodeId]->distance;
				PATH[srcNodeId][dstNodeId].start = pathChain->first;
				PATH[srcNodeId][dstNodeId].end = pathChain->last;
				free(pathChain);
			}
			flowLL = flowLL->next;
		}
	}

	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NUM_NODES; j++) {
			free(S[i][j]);
		}
	}
	free(S);
	free(F);
}





/******************************************************************************/

int main(int argc, char** argv) {

	int i, j;
	unsigned int runNum;
	unsigned int seed;
	unsigned int numEdges, numFlows;
	double timeSimple, timeShinn;
	double simpleLow, simpleHigh, simpleTotal;
	double shinnLow, shinnHigh, shinnTotal;
	clock_t startTime, endTime;
	EDGE** tmpEdgePtrPtr;
	EDGE_LLN* edgeLL;
	FLOW** tmpFlowPtrPtr;
	FLOW_LLN* flowLL;
	PATH* tmpPathPtr;
	PATH** PATH1;
	PATH** PATH2;
	PATH_NODE* pathNode;
	PATH_NODE* nextPathNode;	

	if (argc != 5) {
		printf("Usage: %s <NUM_NODES> <MULTIPLIER> <MAX_CAP> <NUM_RUNS>\n", argv[0]);
		return 0;
	}
	NUM_NODES = atoi(argv[1]);
	NUM_EDGES = atoi(argv[2]) * NUM_NODES;
	if (NUM_EDGES > ((NUM_NODES * NUM_NODES) - NUM_NODES)) {
		NUM_EDGES = (NUM_NODES * NUM_NODES) - NUM_NODES;
	}
	MAX_CAP   = atoi(argv[3]);
	NUM_RUNS  = atoi(argv[4]);
	seed = time(NULL);
	/* seed = 1361845750; */
	srand(seed);
	printf("\nNUM_NODES = %d, NUM_EDGES = %d, MAX_CAP = %d, NUM_RUNS = %d, seed = %d\n", NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS, seed);

	/* Initialize 2D arrays of pointers to edges and flows */
	EDGES = (EDGE***) malloc(NUM_NODES * sizeof(EDGE**));
	tmpEdgePtrPtr = (EDGE**) malloc(NUM_NODES * NUM_NODES * sizeof(EDGE*));
	for (i = 0; i < NUM_NODES; i++) {
		EDGES[i] = tmpEdgePtrPtr + (i * NUM_NODES);
	}
	FLOWS = (FLOW***) malloc(NUM_NODES * sizeof(FLOW**));
	tmpFlowPtrPtr = (FLOW**) malloc(NUM_NODES * NUM_NODES * sizeof(FLOW*));
	for (i = 0; i < NUM_NODES; i++) {
		FLOWS[i] = tmpFlowPtrPtr + (i * NUM_NODES);
	}

	/* Initialize buckets for sorting flows and edges by their capacities and bandwidth requirements */
	FLOW_BUCKET = (FLOW_LLN**) malloc((MAX_CAP + 1) * sizeof(FLOW_LLN*));
	EDGE_BUCKET = (EDGE_LLN**) malloc((MAX_CAP + 1) * sizeof(EDGE_LLN*));
	for (i = 0; i < MAX_CAP; i++) {
		FLOW_BUCKET[i] = NULL;
		EDGE_BUCKET[i] = NULL;
	}

	/* Initialize the structure for storing the resulting paths for all flows */
	PATH1 = (PATH**) malloc(NUM_NODES * sizeof(PATH*));
	tmpPathPtr = (PATH*) malloc(NUM_NODES * NUM_NODES * sizeof(PATH));
	for (i = 0; i < NUM_NODES; i++) {
		PATH1[i] = tmpPathPtr + (i * NUM_NODES);
	}
	PATH2 = (PATH**) malloc(NUM_NODES * sizeof(PATH*));
	tmpPathPtr = (PATH*) malloc(NUM_NODES * NUM_NODES * sizeof(PATH));
	for (i = 0; i < NUM_NODES; i++) {
		PATH2[i] = tmpPathPtr + (i * NUM_NODES);
	}

	/* Main Loop */
	simpleLow   = 0;
	simpleHigh  = 0;
	simpleTotal = 0;
	shinnLow    = 0;
	shinnHigh   = 0;
	shinnTotal  = 0;
	for (runNum = 0; runNum <= NUM_RUNS; runNum++) {

		printf("\n================ RUN #%d ================\n", runNum);

		/* Initialize structures and generate random capacities/bandwidths */
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				EDGES[i][j] = NULL;
				FLOWS[i][j] = NULL;
			}
		}
		numFlows = 0;
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (i != j) {
					numFlows++;
					FLOWS[i][j] = (FLOW*) malloc(sizeof(FLOW));
					FLOWS[i][j]->srcNodeId = i;
					FLOWS[i][j]->dstNodeId = j;
					FLOWS[i][j]->bandwidth = (unsigned int) rand() % MAX_CAP + 1;
				}
			}
		}
		numEdges = 0;
		while (numEdges < NUM_EDGES) {
			i = rand() % NUM_NODES;
			j = rand() % NUM_NODES;
			if (i != j) {
				if (EDGES[i][j] == NULL) {
					numEdges++;
					EDGES[i][j] = (EDGE*) malloc(sizeof(EDGE));
					EDGES[i][j]->srcNodeId = i;
					EDGES[i][j]->dstNodeId = j;
					EDGES[i][j]->capacity = (unsigned int) rand() % MAX_CAP + 1;
				}
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				PATH1[i][j].distance = 0;
				PATH1[i][j].start = NULL;
				PATH1[i][j].end = NULL;
				PATH2[i][j].distance = 0;
				PATH2[i][j].start = NULL;
				PATH2[i][j].end = NULL;
				if (EDGES[i][j] != NULL) {
					edgeLL = (EDGE_LLN*) malloc(sizeof(EDGE_LLN));
					edgeLL->edge = EDGES[i][j];
					edgeLL->next = EDGE_BUCKET[EDGES[i][j]->capacity];
					EDGE_BUCKET[EDGES[i][j]->capacity] = edgeLL;
				}
				if (FLOWS[i][j] != NULL) {
					flowLL = (FLOW_LLN*) malloc(sizeof(FLOW_LLN));
					flowLL->flow = FLOWS[i][j];
					flowLL->next = FLOW_BUCKET[FLOWS[i][j]->bandwidth];
					FLOW_BUCKET[FLOWS[i][j]->bandwidth] = flowLL;
				}
			}
		}

		/* Run the simple algorithm followed by the new algorithm */
		startTime = clock();
		simple_algorithm(PATH1);
		endTime = clock();
		timeSimple = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		if (runNum > 0) {
			simpleTotal = simpleTotal + timeSimple;
			if (simpleLow == 0 || simpleLow > timeSimple) {
				simpleLow = timeSimple;
			}
			if (timeSimple > simpleHigh) {
				simpleHigh = timeSimple;
			}
		}
		printf("Simple Algorithm  : %.3f seconds\n", timeSimple);

		startTime = clock();
		shinn_algorithm(PATH2);
		endTime = clock();
		timeShinn = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		if (runNum > 0) {
			shinnTotal = shinnTotal + timeShinn;
			if (shinnLow == 0 || shinnLow > timeShinn) {
				shinnLow = timeShinn;
			}
			if (shinnHigh < timeShinn) {
				shinnHigh = timeShinn;
			}
		}
		printf("Shinn's Algorithm : %.3f seconds\n", timeShinn);

		/* Check results */
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (i != j) {
					pathNode = PATH2[i][j].start;
					while (pathNode != NULL) {
						nextPathNode = pathNode->next;
						if (nextPathNode == NULL) {
							break;
						}
						if (pathNode->nodeId != nextPathNode->nodeId) {
							if (EDGES[pathNode->nodeId][nextPathNode->nodeId]->capacity < FLOWS[i][j]->bandwidth) {
								printf("!!! %d -> %d CAPACITY ERROR !!!\n", pathNode->nodeId, nextPathNode->nodeId);
							}
						}
						pathNode = nextPathNode;
					}
				}
			}

		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (PATH1[i][j].distance != PATH2[i][j].distance) {
					printf("!!! %d -> %d DISTANCE ERROR!!!\n", i, j);
				}
			}
		}

		/* Clean up for the next run */
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				free(EDGES[i][j]);
				free(FLOWS[i][j]);
			}
		}
		for (i = 0; i <= MAX_CAP; i++) {
			while (EDGE_BUCKET[i] != NULL) {
				edgeLL = EDGE_BUCKET[i];
				EDGE_BUCKET[i] = edgeLL->next;
				free(edgeLL);
			}
			while (FLOW_BUCKET[i] != NULL) {
				flowLL = FLOW_BUCKET[i];
				FLOW_BUCKET[i] = flowLL->next;
				free(flowLL);
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				while (PATH1[i][j].start != NULL) {
					pathNode = PATH1[i][j].start;
					PATH1[i][j].start = pathNode->next;
					free(pathNode);
				}
			}
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				while (PATH2[i][j].start != NULL) {
					pathNode = PATH2[i][j].start;
					PATH2[i][j].start = pathNode->next;
					free(pathNode);
				}
			}
		}
	}

	printf("\n");
	printf("Simple - Lowest: %.3f, Highest: %.3f, Average: %.3f\n", simpleLow, simpleHigh, simpleTotal / NUM_RUNS);
	printf("Shinn  - Lowest: %.3f, Highest: %.3f, Average: %.3f\n", shinnLow, shinnHigh, shinnTotal / NUM_RUNS);
	
	return 0;
}

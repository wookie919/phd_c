#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define N 1024

int main(int argc, char *argv[]) {
	MPI_Comm cannon_comm;
	MPI_Status status;
	int rank, size;
	int shift;
	int i, j, k;

	int dims[2], periods[2], coord[2];

	int row, col;
	int halfLeft, halfRight, halfUp, halfDown;
	int left, right, up, down;
	int *A, *B, *A2, *B2, *Am, *Bm, *C, *D;

	int *recvBuf, *recvBuf2, *tmp;

	double start, end;
	int seed;
	int M;
	int Nl;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	seed = time(NULL);
	srand(seed);
	
	dims[0] = 0;
	dims[1] = 0;
	periods[0] = 1;
	periods[1] = 1;

	MPI_Dims_create(size, 2, dims);

	if(dims[0] != dims[1]) {
		if(rank == 0) {
			printf("The number of processors must be a perfect square.\n");
		}
		MPI_Finalize();
		return 0;
	}
	
	M = dims[0];
	Nl = N / M;

	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &cannon_comm);
	MPI_Cart_shift(cannon_comm, 0, 1, &left, &right);
	MPI_Cart_shift(cannon_comm, 1, 1, &up, &down);
	MPI_Cart_shift(cannon_comm, 0, M / 2, &halfLeft, &halfRight);
	MPI_Cart_shift(cannon_comm, 1, M / 2, &halfUp, &halfDown);

	A = (int*) malloc(Nl * Nl * sizeof(int));
	B = (int*) malloc(Nl * Nl * sizeof(int));
	A2 = (int*) malloc(2 * Nl * Nl * sizeof(int));
	B2 = (int*) malloc(2 * Nl * Nl * sizeof(int));
	C = (int*) malloc(Nl * Nl * sizeof(int));
	D = (int*) malloc(Nl * Nl * sizeof(int));
	recvBuf = (int*) malloc (Nl * Nl * sizeof(int));
	recvBuf2 = (int*) malloc(2 * Nl * Nl * sizeof(int));

	for(i = 0; i < Nl; i++) {
		for(j = 0; j < Nl; j++) {
			A[(i * Nl) + j] = rand() % 10;
			B[(i * Nl) + j] = rand() % 10;
			A2[(i * Nl) + j] = A[(i * Nl) + j];
			B2[(i * Nl) + j] = B[(i * Nl) + j];
			C[(i * Nl) + j] = 0;
			D[(i * Nl) + j] = 0;
		}
	}

	// Original Cannon's n step algorithm.
	start = MPI_Wtime();
	for(shift = 0; shift < M; shift++) {
		for(i = 0; i < Nl; i++) {
			for(k = 0; k < Nl; k++) {
				for(j = 0; j < Nl; j++) {
					C[(i * Nl) + j] += A[(i * Nl) + k] * B[(k * Nl) + j];
				}
			}
		}
		if(shift == M - 1) {
			break;
		}

		MPI_Sendrecv(A, Nl * Nl, MPI_INT, right, 1, recvBuf, Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBuf;
		recvBuf = A;
		A = tmp;

		MPI_Sendrecv(B, Nl * Nl, MPI_INT, down, 2, recvBuf, Nl * Nl, MPI_INT, up, 2, MPI_COMM_WORLD, &status);
		tmp = recvBuf;
		recvBuf = B;
		B = tmp;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	end = MPI_Wtime();
	
	if (rank == 0) {
		printf("Time: %.4fs\n", end - start);
	}

	// Our new n/2 algorithm.
	Am = A2 + (Nl * Nl);
	Bm = B2 + (Nl * Nl);
	MPI_Sendrecv(A2, Nl * Nl, MPI_INT, halfLeft, 1, Am, Nl * Nl, MPI_INT, halfLeft, 1, MPI_COMM_WORLD, &status);
	MPI_Sendrecv(B2, Nl * Nl, MPI_INT, halfDown, 1, Bm, Nl * Nl, MPI_INT, halfDown, 1, MPI_COMM_WORLD, &status);

	start = MPI_Wtime();
	// Main loop of the new algorithm, taking M / 2 steps.
	for (shift = 0; shift < M / 2; shift++) {
		for (i = 0; i < Nl; i++) {
			for (k = 0; k < Nl; k++) {
				for (j = 0; j < Nl; j++) {
					D[(i * Nl) + j] += A2[(i * Nl) + k] * B2[(k * Nl) + j] + Am[(i * Nl) + k] * Bm[(k * Nl) + j];
				}
			}
		}
		if (shift == (M / 2) - 1) {
			break;
		}

		MPI_Sendrecv(A2, 2 * Nl * Nl, MPI_INT, right, 1, recvBuf2, 2 * Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBuf2;
		recvBuf2 = A2;
		A2 = tmp;
		Am = A2 + (Nl * Nl);
		
		MPI_Sendrecv(B2, 2 * Nl * Nl, MPI_INT, right, 1, recvBuf2, 2 * Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBuf2;
		recvBuf2 = B2;
		B2 = tmp;
		Bm = B2 + (Nl * Nl);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	end = MPI_Wtime();
	
	if (rank == 0) {
		printf("Time: %.4fs\n", end - start);
	}
	
	// Check the results
	for (i = 0; i < Nl; i++) {
		for (j = 0; j < Nl; j++) {
			if (C[(i * Nl) + j] != D[(i * Nl) + j]) {
				printf("We have a problem\n");
			}
		}
	}
	
	free(A);
	free(B);
	free(A2);
	free(B2);
	free(C);
	free(D);
	free(recvBuf);
	free(recvBuf2);
	
	MPI_Finalize();
	return 0;
}


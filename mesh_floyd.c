#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mpi.h"

#define TRUE 1
#define FALSE 0

#define DEBUG FALSE

#define LEFT 0
#define RIGHT 1
#define UP 2
#define DOWN 3

#define INF 99

#define min(a,b) \
	({__typeof__ (a) _a = (a); \
	  __typeof__ (b) _b = (b); \
	  _a < _b ? _a : _b; })


int main(int argc, char ** argv) {

	unsigned char failed;

	unsigned int i, j, k, l, p, q;
	unsigned int RANK, SIZE;
	unsigned int step = 0;
	unsigned int N, R;
	unsigned int rowFrom, rowTo, colFrom, colTo;
	unsigned int cell;
	
	int ROW, COL;
	int ROW_U, ROW_D, COL_L, COL_R;
	int RANK_U, RANK_D, RANK_L, RANK_R;
	
	int *tmpIntPtr;
	int **D; /* N x N distance matrix, will be solved with mesh. */
	int **E; /* Contains same data as D, will be solved with Floyd-Warshall. */
	int **C; /* R x R distance matrix, one per Cell. */

	int **dataFromU, **dataFromD, **dataFromL, **dataFromR;
	int **dataToU, **dataToD, **dataToL, **dataToR;
	int signalFromU, signalFromD, signalFromL, signalFromR;
	int signalToU, signalToD, signalToL, signalToR;
	int flagFromU, flagFromD, flagFromL, flagFromR;
	int flagToU, flagToD, flagToL, flagToR;
	
	unsigned long int timeDiff;
	
	MPI_Status status;

	struct timeval tvBegin, tvEnd, tvDiff;

	/* Check parameters and initialize all processes */
	if (argc != 3) {
		printf("Usage: %s <n> <r>\n", argv[0]);
		return 0;
	}
	N = atoi(argv[1]);
	R = atoi(argv[2]);
	if (N % R > 0) {
		printf("n must be divisible by r\n");
		return 0;
	}
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	if ((N/R) * (N/R) != SIZE - 1) {
		printf("Number of processors must be 1 greater than (n/r)^2\n");
		return 0;
	}

	/***************************************************************************
	 *
	 * Code for MASTER
	 *
	 **************************************************************************/
	if (RANK == SIZE - 1) {
		if (DEBUG) printf("MASTER: Initializing the distance matrix:\n");
		D = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(N * N * sizeof(int));
		for (i = 0; i < N; i++) {
			D[i] = tmpIntPtr + (i * N);
			for (j = 0; j < N; j++) {
				D[i][j] = INF;
			}
		}
		E = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(N * N * sizeof(int));
		for (i = 0; i < N; i++) {
			E[i] = tmpIntPtr + (i * N);
		}
		/* Initialize diagonals to zero */
		for (i = 0; i < N; i++) {
			D[i][i] = 0;
		}
		/* Start with a ring */
		for (i = 0; i < N - 1; i++) {
			D[i][i + 1] = 1;
		}
		D[N - 1][0] = 1;
		
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				E[i][j] = D[i][j];
			}
		}
		
		if (DEBUG) {
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", D[i][j]);
				}
				printf("\n");
			}
		}
		
		if (DEBUG) printf("MASTER: communicating the relevant portions of the distance matrix to all Cells\n");
		C = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) {
			C[i] = tmpIntPtr + (i * R);
		}
		for (i = 0; i < N/R; i++) {
			for (j = 0; j < N/R; j++) {
				rowFrom = i * R;
				rowTo = rowFrom + R;
				colFrom = j * R;
				colTo = colFrom + R;
				
				k = 0;
				for (p = rowFrom; p < rowTo; p++) {
					l = 0;
					for (q = colFrom; q < colTo; q++) {
						C[k][l] = D[p][q];
						l++;
					}
					k++;
				}
				
				cell = (i * N/R) + j;
				if (DEBUG) printf("MASTER: sending to Cell(%d,%d) (rank: %d)\n", i, j, cell);
				MPI_Send(&(C[0][0]), R * R, MPI_INT, cell, step, MPI_COMM_WORLD);
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
		
		/***************************
		 * MAIN LOOP for the MASTER
		 ***************************/
		gettimeofday(&tvBegin, NULL);
		while (step < 5 * N/R - 5) {
			if (DEBUG) {
				for (i = 0; i < N/R; i++) {
					for (j = 0; j < N/R; j++) {
						cell = (i * N/R) + j;
						MPI_Recv(&(C[0][0]), R * R, MPI_INT, cell, step, MPI_COMM_WORLD, &status);
						k = i * R;
						for (p = 0; p < R; p++) {
							l = j * R;
							for (q = 0; q < R; q++) {
								D[k][l] = C[p][q];
								l++;
							}
							k++;
						}
					}
				}
				printf("MASTER: Distances after step %d\n", step);
				for (i = 0; i < N; i++) {
					for (j = 0; j < N; j++) {
						printf("%d ", D[i][j]);
					}
					printf("\n");
				}
			}
			MPI_Barrier(MPI_COMM_WORLD);
			step++;
		}
		gettimeofday(&tvEnd, NULL);
		
		timeDiff = (tvEnd.tv_usec + 1000000 * tvEnd.tv_sec) - (tvBegin.tv_usec + 1000000 * tvBegin.tv_sec);
		printf("Time taken: %ld Microseconds\n", timeDiff);

		if (DEBUG) {
			printf("MASTER: solve with Floyd-Warshall\n");
			for (k = 0; k < N; k++) {
				for (i = 0; i < N; i++) {
					for (j = 0; j < N; j++) {
						E[i][j] = min(E[i][j], E[i][k] + E[k][j]);
					}
				}
			}
		
			failed = FALSE;
			printf("MASTER: compare D and E\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					if (D[i][j] != E[i][j]) {
						printf("MASTER: D[%d][%d]: %d, E[%d][%d]: %d\n", i, j, D[i][j], i, j, E[i][j]);
						failed = TRUE;
						break;
					}
				}
				if (failed) {
					break;
				}
			}
		}
	}

	/***************************************************************************
	 *
	 * Code for all Cells (MASTER is not a Cell)
	 *
	 **************************************************************************/
	else {
		ROW = RANK / (N/R);
		COL = RANK % (N/R);

		ROW_U = ROW - 1;
		ROW_D = ROW + 1;
		COL_L = COL - 1;
		COL_R = COL + 1;
		
		/* Wrap around is easier than handling corner cases for MPI_send/recv */
		if (ROW_U < 0) ROW_U = N/R - 1;
		if (ROW_D == N/R) ROW_D = 0;
		if (COL_L < 0) COL_L = N/R - 1;
		if (COL_R == N/R) COL_R = 0;

		/* Determine the rank of neighbours from ROW/COL */
		RANK_U = ROW_U * N/R + COL;
		RANK_D = ROW_D * N/R + COL;
		RANK_L = ROW * N/R + COL_L;
		RANK_R = ROW * N/R + COL_R;

		if (DEBUG) printf("Cell(%d,%d): Initializing communication channels\n", ROW, COL);
		dataFromU = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataFromU[i] = tmpIntPtr + (i * R);
		dataFromD = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataFromD[i] = tmpIntPtr + (i * R);
		dataFromL = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataFromL[i] = tmpIntPtr + (i * R);
		dataFromR = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataFromR[i] = tmpIntPtr + (i * R);
		dataToU = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataToU[i] = tmpIntPtr + (i * R);
		dataToD = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataToD[i] = tmpIntPtr + (i * R);
		dataToL = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataToL[i] = tmpIntPtr + (i * R);
		dataToR = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) dataToR[i] = tmpIntPtr + (i * R);
		signalFromU = FALSE;
		signalFromD = FALSE;
		signalFromL = FALSE;
		signalFromR = FALSE;
		flagFromU = FALSE;
		flagFromD = FALSE;
		flagFromL = FALSE;
		flagFromR = FALSE;

		C = (int**) malloc(R * sizeof(int*));
		tmpIntPtr = (int*) malloc(R * R * sizeof(int));
		for (i = 0; i < R; i++) C[i] = tmpIntPtr + (i * R);
		MPI_Recv(&(C[0][0]), R * R, MPI_INT, SIZE - 1, step, MPI_COMM_WORLD, &status);
		if (DEBUG) printf("Cell(%d,%d): Received data from MASTER\n", ROW, COL);
		MPI_Barrier(MPI_COMM_WORLD);
		
		/**************************
		 * MAIN LOOP for all Cells
		 **************************/
		while (step < 5 * N/R - 5) {
		
			/* Prepare to send/recv by copying data from "dataFrom" to "dataTo" */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					dataToR[i][j] = dataFromL[i][j];
					dataToD[i][j] = dataFromU[i][j];
					dataToL[i][j] = dataFromR[i][j];
					dataToU[i][j] = dataFromD[i][j];
				}
			}
			signalToR = signalFromL;
			signalToD = signalFromU;
			signalToL = signalFromR;
			signalToU = signalFromD;
			flagToR = flagFromL;
			flagToD = flagFromU;
			flagToL = flagFromR;
			flagToU = flagFromD;
		
			/* Trigger control signals at step % 3 */
			if ((step % 3) == 0) {
				if ((ROW == step / 3) && (COL == step / 3)) {
					/* Need to perform Floyd within cell to begin with. */
					for (k = 0; k < R; k++) {
						for (i = 0; i < R; i++) {
							for (j = 0; j < R; j++) {
								C[i][j] = min(C[i][j], C[i][k] + C[k][j]);
							}
						}
					}
					if (DEBUG) printf("Cell(%d,%d): Triggering control signals\n", ROW, COL);
					signalToL = TRUE;
					signalToU = TRUE;
					signalToR = TRUE;
					signalToD = TRUE;
					flagToL = TRUE;
					flagToR = TRUE;
					flagToD = TRUE;
					flagToU = TRUE;
					/* Need to send data alongside the signals */
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							dataToR[i][j] = C[i][j];
							dataToD[i][j] = C[i][j];
							dataToL[i][j] = C[i][j];
							dataToU[i][j] = C[i][j];
						}
					}
				}
			}

			/* If signal was received then data from C needs to be copied for transfer */
			if (signalFromL || signalFromR) {
				flagToU = TRUE;
				flagToD = TRUE;
				for (i = 0; i < R; i++) {
					for (j = 0; j < R; j++) {
						dataToU[i][j] = C[i][j];
						dataToD[i][j] = C[i][j];
					}
				}
			}
			if (signalFromU || signalFromD) {
				flagToR = TRUE;
				flagToL = TRUE;
				for (i = 0; i < R; i++) {
					for (j = 0; j < R; j++) {
						dataToR[i][j] = C[i][j];
						dataToL[i][j] = C[i][j];
					}
				}
			}
			
			/* Send to Right, Receive from Left. */
			if (RANK_L > RANK) {
				MPI_Send(&signalToR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				MPI_Send(&flagToR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				MPI_Send(&(dataToR[0][0]), R * R, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				signalFromL = FALSE;
				flagFromL = FALSE;
			}
			else if (RANK_R < RANK) {
				MPI_Recv(&signalFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&flagFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&(dataFromL[0][0]), R * R, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&signalToR, 1, MPI_INT, RANK_R, step,
					&signalFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&flagToR, 1, MPI_INT, RANK_R, step,
					&flagFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&(dataToR[0][0]), R * R, MPI_INT, RANK_R, step,
					&(dataFromL[0][0]), R * R, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
			}
			
			/* Send to Down, Receive from Up. */
			if (RANK_U > RANK) {
				MPI_Send(&signalToD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD);
				MPI_Send(&flagToD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD);
				MPI_Send(&(dataToD[0][0]), R * R, MPI_INT, RANK_D, step, MPI_COMM_WORLD);
				signalFromU = FALSE;
				flagFromU = FALSE;
			}
			else if (RANK_D < RANK) {
				MPI_Recv(&signalFromU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&flagFromU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&(dataFromU[0][0]), R * R, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&signalToD, 1, MPI_INT, RANK_D, step,
					&signalFromU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&flagToD, 1, MPI_INT, RANK_D, step,
					&flagFromU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&(dataToD[0][0]), R * R, MPI_INT, RANK_D, step,
					&(dataFromU[0][0]), R * R, MPI_INT, RANK_U, step, MPI_COMM_WORLD, &status);
			}
			
			/* Send to Left, Receive from Right. */
			if (RANK_R < RANK) {
				MPI_Send(&signalToL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				MPI_Send(&flagToL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				MPI_Send(&(dataToL[0][0]), R * R, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				signalFromR = FALSE;
				flagFromR = FALSE;
			}
			else if (RANK_L > RANK) {
				MPI_Recv(&signalFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&flagFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&(dataFromR[0][0]), R * R, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&signalToL, 1, MPI_INT, RANK_L, step,
					&signalFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&flagToL, 1, MPI_INT, RANK_L, step,
					&flagFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&(dataToL[0][0]), R * R, MPI_INT, RANK_L, step,
					&(dataFromR[0][0]), R * R, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
			}

			/* Send to Up, Receive from Down. */
			if (RANK_D < RANK) {
				MPI_Send(&signalToU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD);
				MPI_Send(&flagToU, 1, MPI_INT, RANK_U, step, MPI_COMM_WORLD);
				MPI_Send(&(dataToU[0][0]), R * R, MPI_INT, RANK_U, step, MPI_COMM_WORLD);
				signalFromD = FALSE;
				flagFromD = FALSE;
			}
			else if (RANK_U > RANK) {
				MPI_Recv(&signalFromD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&flagFromD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&(dataFromD[0][0]), R * R, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&signalToU, 1, MPI_INT, RANK_U, step,
					&signalFromD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&flagToU, 1, MPI_INT, RANK_U, step,
					&flagFromD, 1, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&(dataToU[0][0]), R * R, MPI_INT, RANK_U, step,
					&(dataFromD[0][0]), R * R, MPI_INT, RANK_D, step, MPI_COMM_WORLD, &status);
			}
			
			/* Process new data. */
			if (flagFromL && flagFromU) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							C[i][j] = min(C[i][j], dataFromL[i][k] + dataFromU[k][j]);
						}
					}
				}
			}
			if (flagFromU && flagFromR) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							C[i][j] = min(C[i][j], dataFromR[i][k] + dataFromU[k][j]);
						}
					}
				}
			}
			if (flagFromR && flagFromD) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							C[i][j] = min(C[i][j], dataFromR[i][k] + dataFromD[k][j]);
						}
					}
				}
			}
			if (flagFromD && flagFromL) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							C[i][j] = min(C[i][j], dataFromL[i][k] + dataFromD[k][j]);
						}
					}
				}
			}
			if (flagFromL && !flagFromU && !flagFromR && !flagFromD) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						if (i != k) {
							for (j = 0; j < R; j++) {
								C[i][j] = min(C[i][j], dataFromL[i][k] + C[k][j]);
							}
						}
					}
				}
			}
			if (flagFromU && !flagFromR && !flagFromD && !flagFromL) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							if (j != k) {
								C[i][j] = min(C[i][j], C[i][k] + dataFromU[k][j]);
							}
						}
					}
				}
			}
			if (flagFromR && !flagFromD && !flagFromL && !flagFromU) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						if (i != k) {
							for (j = 0; j < R; j++) {
								C[i][j] = min(C[i][j], dataFromR[i][k] + C[k][j]);
							}
						}
					}
				}
			}
			if (flagFromD && !flagFromL && !flagFromU && !flagFromR) {
				for (k = 0; k < R; k++) {
					for (i = 0; i < R; i++) {
						for (j = 0; j < R; j++) {
							if (j != k) {
								C[i][j] = min(C[i][j], C[i][k] + dataFromD[k][j]);
							}
						}
					}
				}
			}
			
			/* Send the current distances to the MASTER at the end of each step for debugging purposes */
			if (DEBUG) MPI_Send(&(C[0][0]), R * R, MPI_INT, SIZE - 1, step, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			step++;
		}
	}

	MPI_Finalize();
	
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0

/* Using the same variable names as the Takaoka '92 paper, capitalized */
#define N   81
#define M   9
#define L   3
#define MU  18   /* M * 2 */
#define K   3    /* (L * (L - 1)) / 2 */
#define LTS 5832 /* MU^K */

static unsigned char I[LTS][LTS]; /* Lookup table */

typedef struct _diff {
	double diff; /* a_ir - a_is */
	unsigned int rank; /* relative rank of this diff in Grs */
} DIFF;

typedef struct _diffs {
	double diff;
	DIFF* diffPtr;
} DIFFS;


/*
 * Perform DMM (C = A x B) in O(n^3) time.
 */
void naive(double A[M][L], double B[L][M], double C[M][M]) {

	unsigned int i, j, k;
	double min;

	for (i = 0; i < M; i++) {
		for (j = 0; j < M; j++) {
			min = C[i][j];
			for (k = 0; k < L; k++) {
				if (A[i][k] + B[k][j] < min) {
					min = A[i][k] + B[k][j];
				}
			}
			C[i][j] = min;
		}
	}
	
	return;
}


int comp_diff(const void* a, const void* b) {
	DIFF* diffA = (DIFF*) a;
	DIFF* diffB = (DIFF*) b;

	if (diffA->diff < diffB->diff) {
		return -1;
	}
	else if (diffA->diff > diffB->diff) {
		return 1;
	}
	else {
		return 0;
	}
}

unsigned long long int ipow(int base, int exp) {
	unsigned long long int result = 1;
	while (exp) {
		if (exp & 1) {
			result *= base;
		}
		exp >>= 1;
		base *= base;
	}
	return result;
}


/*
 * Perform DMM (C = A x B) in O(n^3 / (loglogn/logn)^(1/2)).
 */
void takaoka92(double A[M][L], double B[L][M], double C[M][M]) {
	
	unsigned int r, s, i, j, k;
	unsigned long long int base, maxBase;
	unsigned long long int encH, encL;
	unsigned long long int H_enc[M], L_enc[M];
	DIFF Hrs[L][L][M], Lrs[L][L][M];
	DIFFS Grs[L][L][MU];
	
	/* Calculate diffs (a_ir - a_is) and (b_sj - b_rj) */
	for (r = 0; r < L - 1; r++) {
		for (s = r + 1; s < L; s++) {
			for (i = 0; i < M; i++) {
				Hrs[r][s][i].diff = A[i][r] - A[i][s];
				Lrs[r][s][i].diff = B[s][i] - B[r][i];
			}
		}
	}
	
	/* Merge the diffs and sort. */
	for (r = 0; r < L - 1; r++) {
		for (s = r + 1; s < L; s++) {
			for (i = 0; i < M; i++) {
				Grs[r][s][i * 2].diff = Hrs[r][s][i].diff;
				Grs[r][s][i * 2].diffPtr = &(Hrs[r][s][i]);
				Grs[r][s][i * 2 + 1].diff = Lrs[r][s][i].diff;
				Grs[r][s][i * 2 + 1].diffPtr = &(Lrs[r][s][i]);
			}
			qsort(Grs[r][s], MU, sizeof(DIFF), comp_diff);
		}
	}
	
	/* After sorting we know what the ranks of each diffs are. */
	for (r = 0; r < L - 1; r++) {
		for (s = r + 1; s < L; s++) {
			for (i = 0; i < MU; i++) {
				Grs[r][s][i].diffPtr->rank = i;
				/* printf("%.2f ", Grs[r][s][i].diff); */
			}
			/* printf("\n"); */
		}
	}

	/* Encode the ranks into a number */
	maxBase = LTS / MU;
	for (i = 0; i < M; i++) {
		base = maxBase;
		encH = 0;
		encL = 0;
		for (r = 0; r < L - 1; r++) {
			for (s = r + 1; s < L; s++) {
				encH = encH + (base * Hrs[r][s][i].rank);
				encL = encL + (base * Lrs[r][s][i].rank);
				base = base / MU;
			}
		}
		H_enc[i] = encH;
		L_enc[i] = encL;
	}

	/* Now we can use H_enc and L_enc to lookup the table */
	for (i = 0; i < M; i++) {
		for (j = 0; j < M; j++) {
			k = I[H_enc[i]][L_enc[j]];
			C[i][j] = A[i][k] + B[k][j];
		}
	}

	return;
}


void decode_into_matrix(unsigned int num, unsigned int matrix[L][L]) {
	int i, j, idx;
	unsigned int rankList[K];
	
	for (i = 0; i < K; i++) {
		rankList[i] = 0;
	}
	
	for (i = K - 1; i >= 0; i--) {
		rankList[i] = num % MU;
		num = num / MU;
	}
	
	idx = 0;
	for (i = 0; i < L - 1; i++) {
		for (j = i + 1; j < L; j++) {
			matrix[i][j] = rankList[idx];
			idx++;
		}
	}
	
	return;
}


int compute_via_node(unsigned int H_matrix[L][L], unsigned int L_matrix[L][L]) {
	unsigned char isK;
	unsigned int k, s, r;

	for (k = 0; k < L; k++) {
		isK = TRUE;
		for (s = k + 1; s < L; s++) {
			if (H_matrix[k][s] >= L_matrix[k][s]) {
				isK = FALSE;
				break;
			}
		}
		for (r = 0; r < k; r++) {
			if (H_matrix[r][k] <= L_matrix[r][k]) {
				isK = FALSE;
				break;
			}
		}
		if (isK) {
			return k;
		}
	}

	return -1;
}


void compute_lookup_table() {
	
	unsigned int i, j;
	unsigned int H_matrix[L][L], L_matrix[L][L];
	
	for (i = 0; i < L; i++) {
		for (j = 0; j < L; j++) {
			H_matrix[i][j] = 0;
			L_matrix[i][j] = 0;
		}
	}
	
	for (i = 0; i < LTS; i++) {
		decode_into_matrix(i, H_matrix);
		for (j = 0; j < LTS; j++) {
			decode_into_matrix(j, L_matrix);
			I[i][j] = compute_via_node(H_matrix, L_matrix);
		}
	}
	
	return;
}


/******************************************************************************/

int main(int argc, char** argv) {
	unsigned int i, j;
	unsigned int seed;
	unsigned int runNum, NUM_RUNS;
	double A[M][L], B[L][M];
	double C1[M][M], C2[M][M];
	double time1, time2;
	clock_t startTime, endTime;

	if (argc != 2) {
		printf("Usage: %s <NUM_RUNS>\n", argv[0]);
		return 0;
	}
	NUM_RUNS = atoi(argv[1]);
	
	seed = time(NULL);
	/* seed = 1361845750; */
	srand(seed);
	printf("n = %d, m = %d, l = %d, seed = %d, numRuns = %d\n", N, M, L, seed, NUM_RUNS);

	/*
	 * IMPORTANT: We assume vertices are numbered from 0 to n - 1.
	 */

	/* Compute the lookup table */
	startTime = clock();
	compute_lookup_table();
	endTime = clock();
	printf("Time taken to compute the lookup table: %f\n", (double) (endTime - startTime) / CLOCKS_PER_SEC);
	
	/* Main Loop */
	for (runNum = 0; runNum < NUM_RUNS; runNum++) {

		/* Initialize C1 and C2 */
		for (i = 0; i < M; i++) {
			for (j = 0; j < M; j++) {
				C1[i][j] = (double) RAND_MAX;
				C2[i][j] = (double) RAND_MAX;
			}
		}

		/* Generate random matrices */
		for (i = 0; i < M; i++) {
			for (j = 0; j < L; j++) {
				A[i][j] = (double) rand() / (double) RAND_MAX;
				B[j][i] = (double) rand() / (double) RAND_MAX;
			}
		}
		
		/* Solve using naive DMM */
		startTime = clock();
		naive(A, B, C1);
		endTime = clock();
		time1 = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		
		/* Solve using Takaoka92 */
		startTime = clock();
		takaoka92(A, B, C2);
		endTime = clock();
		time2 = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		
		for (i = 0; i < M; i++) {
			for (j = 0; j < M; j++) {
				if (C1[i][j] != C2[i][j]) {
					printf("!!! ERROR !!!: C1[%d][%d] = %f, C2[%d][%d] = %f\n", i, j, C1[i][j], i, j, C2[i][j]);
				}
			}
		}
		
		printf("%f %f\n", time1, time2);
	}

	return 0;
}


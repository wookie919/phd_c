#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define N 32
#define K 4

int main(int argc, char *argv[]) {
	MPI_Comm cannon_comm;
	MPI_Status status;
	int rank, size;
	int shift;
	int i, j, k, l;

	int dims[2], periods[2], coord[2];

	int row, col;
	int left, right, up, down;
	int jumpLeft, jumpRight, jumpUp, jumpDown;
	int *A, *B, *AK, *BK, *Ap, *Bp, *C, *D;

	int *recvBuf, *recvBufK, *tmp;

	double start, end;
	int seed;
	int M, Nl, L;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	seed = time(NULL);
	//seed = 1402466901;
	srand(seed);
	
	dims[0] = 0;
	dims[1] = 0;
	periods[0] = 1;
	periods[1] = 1;

	MPI_Dims_create(size, 2, dims);

	if (dims[0] != dims[1]) {
		if(rank == 0) {
			printf("The number of processors must be a perfect square.\n");
		}
		MPI_Finalize();
		return 0;
	}
	M = dims[0];

	if (N % M != 0) {
		if(rank == 0) {
			printf("N must be divisible by M (number of cells in a row/column).\n");
		}
		MPI_Finalize();
		return 0;
	}
	Nl = N / M;

	if (M % K != 0) {
		if(rank == 0) {
			printf("M must be divisible by K (number of layers).\n");
		}
		MPI_Finalize();
		return 0;
	}
	L = M / K;

	if (rank == 0) {
		printf("seed: %d\n", seed);
	}

	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &cannon_comm);
	MPI_Cart_shift(cannon_comm, 0, 1, &left, &right);
	MPI_Cart_shift(cannon_comm, 1, 1, &up, &down);

	A = (int*) malloc(Nl * Nl * sizeof(int));
	B = (int*) malloc(Nl * Nl * sizeof(int));

	AK = (int*) malloc(K * Nl * Nl * sizeof(int));
	BK = (int*) malloc(K * Nl * Nl * sizeof(int));

	C = (int*) malloc(Nl * Nl * sizeof(int));
	D = (int*) malloc(Nl * Nl * sizeof(int));

	recvBuf = (int*) malloc (Nl * Nl * sizeof(int));
	recvBufK = (int*) malloc(K * Nl * Nl * sizeof(int));

	for(i = 0; i < Nl; i++) {
		for(j = 0; j < Nl; j++) {
			A[(i * Nl) + j] = rand() % 10;
			B[(i * Nl) + j] = rand() % 10;
			AK[(i * Nl) + j] = A[(i * Nl) + j];
			BK[(i * Nl) + j] = B[(i * Nl) + j];
			C[(i * Nl) + j] = 0;
			D[(i * Nl) + j] = 0;
		}
	}

	//==========================================================================
	// Original Cannon's n step algorithm.
	start = MPI_Wtime();
	for(shift = 0; shift < M; shift++) {
		for(i = 0; i < Nl; i++) {
			for(k = 0; k < Nl; k++) {
				for(j = 0; j < Nl; j++) {
					C[(i * Nl) + j] += A[(i * Nl) + k] * B[(k * Nl) + j];
				}
			}
		}
		if(shift == M - 1) {
			break;
		}

		MPI_Sendrecv(A, Nl * Nl, MPI_INT, right, 1, recvBuf, Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBuf;
		recvBuf = A;
		A = tmp;

		MPI_Sendrecv(B, Nl * Nl, MPI_INT, down, 2, recvBuf, Nl * Nl, MPI_INT, up, 2, MPI_COMM_WORLD, &status);
		tmp = recvBuf;
		recvBuf = B;
		B = tmp;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	end = MPI_Wtime();
	
	if (rank == 0) {
		printf("Time: %.4fs\n", end - start);
	}


	//==========================================================================
	// Our new K-layers algorithm. Create staggered layers.
	for (k = 1; k < K; k++) {
		Ap = AK + (k * Nl * Nl);
		Bp = BK + (k * Nl * Nl);
		MPI_Cart_shift(cannon_comm, 0, k * L, &jumpLeft, &jumpRight);
		MPI_Cart_shift(cannon_comm, 1, k * L, &jumpUp, &jumpDown);
		MPI_Sendrecv(AK, Nl * Nl, MPI_INT, jumpLeft, k, Ap, Nl * Nl, MPI_INT, jumpRight, k, MPI_COMM_WORLD, &status);
		MPI_Sendrecv(BK, Nl * Nl, MPI_INT, jumpDown, k, Bp, Nl * Nl, MPI_INT, jumpUp, k, MPI_COMM_WORLD, &status);
	}

	start = MPI_Wtime();
	// Main loop of the new algorithm, taking L = M / K steps.
	for (shift = 0; shift < L; shift++) {
		for (i = 0; i < Nl; i++) {
			for (k = 0; k < Nl; k++) {
				for (j = 0; j < Nl; j++) {
					for (l = 0; l < K; l++) {
						D[(i * Nl) + j] += AK[(l * Nl * Nl) + (i * Nl) + k] * BK[(l * Nl * Nl) + (k * Nl) + j];
					}
				}
			}
		}
		if (shift == L) {
			break;
		}

		MPI_Sendrecv(AK, K * Nl * Nl, MPI_INT, right, 1, recvBufK, K * Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBufK;
		recvBufK = AK;
		AK = tmp;

		MPI_Sendrecv(BK, K * Nl * Nl, MPI_INT, right, 1, recvBufK, K * Nl * Nl, MPI_INT, left, 1, MPI_COMM_WORLD, &status);
		tmp = recvBufK;
		recvBufK = BK;
		BK = tmp;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	end = MPI_Wtime();
	
	if (rank == 0) {
		printf("Time: %.4fs\n", end - start);
	}
	
	// Check the results
	for (i = 0; i < Nl; i++) {
		for (j = 0; j < Nl; j++) {
			if (C[(i * Nl) + j] != D[(i * Nl) + j]) {
				printf("We have a problem!!!\n");
			}
		}
	}

	free(A);
	free(B);
	free(AK);
	free(BK);
	free(C);
	free(D);
	free(recvBuf);
	free(recvBufK);
	
	MPI_Finalize();
	return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define N 256

#define min(a, b) \
	({__typeof__ (a) _a = (a); \
	  __typeof__ (b) _b = (b); \
	  _a < _b ? _a : _b; })

#define INF 1
#define ERR 0.00000000001

#define TRUE 1
#define FALSE 0

struct cell {
	double fromL, fromR, fromT, fromB;
	double toL, toR, toT, toB;
	double val;
	int sigFromL, sigFromR, sigFromT, sigFromB;
	int sigToL, sigToR, sigToB, sigToT;
	unsigned char recL, recR, recT, recB;
};
typedef struct cell cell;


int main(int argc, char *argv[]) {
	MPI_Comm comm;
	MPI_Status status;
	int P;
	int R;
	double *D, *D0;
	cell *CELL;
	int rank, size;
	int shift;
	int i, j, k, testNum;
	int dims[2], periods[2], coords[2];
	int left, right, top, bottom;
	int baseRow, baseCol;
	int step, idx;
	unsigned int seed;
	double *txBuf;
	double *fromB, *fromT, *fromL, *fromR;
	int *txSig;
	int *sigFromB, *sigFromT, *sigFromL, *sigFromR;
	double start, end;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	dims[0] = 0;
	dims[1] = 0;
	MPI_Dims_create(size, 2, dims);
	if(dims[0] != dims[1]) {
		if(rank == 0) {
			printf("The number of processors must be a square.\n");
		}
		MPI_Finalize();
		return 0;
	}
	P = dims[0];
	R = N / P;
	periods[0] = 1;
	periods[1] = 1;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &comm);
	MPI_Cart_shift(comm, 1, 1, &left, &right);
	MPI_Cart_shift(comm, 0, 1, &top, &bottom);
	MPI_Cart_coords(comm, rank, dims[0], coords);
	baseRow = R * coords[0];
	baseCol = R * coords[1];


	for (testNum = 0; testNum < 10; testNum++) {

		if (rank == 0) {
			seed = time(NULL);
			//seed = 1421997791;
			printf("Run #%d, seed: %d\n", testNum, seed);
			for (i = 1; i < P * P; i++) {
				MPI_Send(&seed, 1, MPI_INT, i, 1, comm);
			}
		}
		else {
			MPI_Recv(&seed, 1, MPI_INT, 0, 1, comm, &status);
		}
		srand(seed);
	
		D = (double*) malloc(N * N * sizeof(double));
		D0 = (double*) malloc(N * N * sizeof(double));
		for(i = 0; i < N; i++) {
			for(j = 0; j < N; j++) {
				if (i == j) {
					D[(i * N) + j] = 0;
				}
				else {
					D[(i * N) + j] = (double) rand() / RAND_MAX;
				}
				D0[(i * N) + j] = D[(i * N) + j];
			}
		}
	
		/*
		for (k = 0; k < N; k++) {
			for(i = 0; i < N; i++) {
				for(j = 0; j < N; j++) {
					D0[(i * N) + j] = min(D0[(i * N) + j], D0[(i * N) + k] + D0[(k * N) + j]);
				}
			}
		}
		*/
		
		/*
		if (rank == 0) {
			for(i = 0; i < N; i++) {
				for(j = 0; j < N; j++) {
					printf("%.2f ", D[(i * N) + j]);
				}
				printf("\n");
			}
			printf("================================\n");
		}
		*/

		txBuf = (double*) malloc(R * sizeof(double));
		fromB = (double*) malloc(R * sizeof(double));
		fromT = (double*) malloc(R * sizeof(double));
		fromL = (double*) malloc(R * sizeof(double));
		fromR = (double*) malloc(R * sizeof(double));
		txSig = (int*) malloc(R * sizeof(int));
		sigFromB = (int*) malloc(R * sizeof(int));
		sigFromT = (int*) malloc(R * sizeof(int));
		sigFromL = (int*) malloc(R * sizeof(int));
		sigFromR = (int*) malloc(R * sizeof(int));
		CELL = (cell*) malloc(R * R * sizeof(cell));





		//=========================================================================
		//
		// 4N Algorithm
		//
		//=========================================================================
		for(i = 0; i < R; i++) {
			for(j = 0; j < R; j++) {
				CELL[(i * R) + j].val = D[((i + baseRow) * N) + (j + baseCol)];
				CELL[(i * R) + j].toL = INF;
				CELL[(i * R) + j].toR = INF;
				CELL[(i * R) + j].toT = INF;
				CELL[(i * R) + j].toB = INF;
				CELL[(i * R) + j].fromL = INF;
				CELL[(i * R) + j].fromR = INF;
				CELL[(i * R) + j].fromT = INF;
				CELL[(i * R) + j].fromB = INF;
				CELL[(i * R) + j].sigToL = 0;
				CELL[(i * R) + j].sigToR = 0;
				CELL[(i * R) + j].sigToT = 0;
				CELL[(i * R) + j].sigToB = 0;
				CELL[(i * R) + j].sigFromL = 0;
				CELL[(i * R) + j].sigFromR = 0;
				CELL[(i * R) + j].sigFromT = 0;
				CELL[(i * R) + j].sigFromB = 0;
			}
		}

		start = MPI_Wtime();
		for (step = 0; step <= 4 * N; step++) {

			/* Initiate control signal if required */
			if (baseRow == baseCol && step < 3 * N) {
				if ((step % 3) == 0) {
					if ((step / 3) >= baseRow && (step / 3) <= baseRow + (R - 1)) {
						idx = (step / 3) - baseRow;
						//printf("step: %d, idx = %d, rank: %d\n", step, idx, rank);
						CELL[(idx * R) + idx].sigToR = N / 2 + 1;
						CELL[(idx * R) + idx].sigToL = N / 2 + 1;
						CELL[(idx * R) + idx].sigToT = N / 2 + 1;
						CELL[(idx * R) + idx].sigToB = N / 2 + 1;
					}
				}
			}

			/* Tx Top Row */
			for (j = 0; j < R; j++) {
				txBuf[j] = CELL[j].toT;
				txSig[j] = CELL[j].sigToT;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, top, 1, fromB, R, MPI_DOUBLE, bottom, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[((R - 1) * R) + j].fromB = fromB[j];
			}
			MPI_Sendrecv(txSig, R, MPI_INT, top, 1, sigFromB, R, MPI_INT, bottom, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[((R - 1) * R) + j].sigFromB = sigFromB[j] - 1;
			}
			/* Tx Bottom Row */
			for (j = 0; j < R; j++) {
				txBuf[j] = CELL[((R - 1) * R) + j].toB;
				txSig[j] = CELL[((R - 1) * R) + j].sigToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, bottom, 1, fromT, R, MPI_DOUBLE, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].fromT = fromT[j];
			}
			MPI_Sendrecv(txSig, R, MPI_INT, bottom, 1, sigFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].sigFromT = sigFromT[j] - 1;
			}
			/* Tx Left Column */
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[i * R].toL;
				txSig[i] = CELL[i * R].sigToL;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, left, 1, fromR, R, MPI_DOUBLE, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].fromR = fromR[i];
			}
			MPI_Sendrecv(txSig, R, MPI_INT, left, 1, sigFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].sigFromR = sigFromR[i] - 1;
			}
			/* Tx Right Column */
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[(i * R) + (R - 1)].toR;
				txSig[i] = CELL[(i * R) + (R - 1)].sigToR;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, right, 1, fromL, R, MPI_DOUBLE, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].fromL = fromL[i];
			}
			MPI_Sendrecv(txSig, R, MPI_INT, right, 1, sigFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].sigFromL = sigFromL[i] - 1;
			}
		
			/* Simulate transfer within R * R array */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					if (i > 0) {
						CELL[(i * R) + j].fromT = CELL[((i - 1) * R) + j].toB;
						CELL[(i * R) + j].sigFromT = CELL[((i - 1) * R) + j].sigToB - 1;
					}
					if (i < (R - 1)) {
						CELL[(i * R) + j].fromB = CELL[((i + 1) * R) + j].toT;
						CELL[(i * R) + j].sigFromB = CELL[((i + 1) * R) + j].sigToT - 1;
					}
					if (j > 0) {
						CELL[(i * R) + j].fromL = CELL[(i * R) + (j - 1)].toR;
						CELL[(i * R) + j].sigFromL = CELL[(i * R) + (j - 1)].sigToR - 1;
					}
					if (j < (R - 1)) {
						CELL[(i * R) + j].fromR = CELL[(i * R) + (j + 1)].toL;
						CELL[(i * R) + j].sigFromR = CELL[(i * R) + (j + 1)].sigToL - 1;
					}
				}
			}

			/* Perform the triple operation */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromT + CELL[(i * R) + j].fromL);
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromT + CELL[(i * R) + j].fromR);
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromB + CELL[(i * R) + j].fromL);
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromB + CELL[(i * R) + j].fromR);
				}
			}

			/* Set the stage for the data transfer in the follownig step */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					if (CELL[(i * R) + j].fromB < INF && CELL[(i * R) + j].fromT < INF) {
						CELL[(i * R) + j].toT = INF;
						CELL[(i * R) + j].toB = INF;
					}
					else {
						CELL[(i * R) + j].toT = CELL[(i * R) + j].fromB;
						CELL[(i * R) + j].toB = CELL[(i * R) + j].fromT;
					}
					if (CELL[(i * R) + j].fromL < INF && CELL[(i * R) + j].fromR < INF) {
						CELL[(i * R) + j].toR = INF;
						CELL[(i * R) + j].toL = INF;
					}
					else {
						CELL[(i * R) + j].toL = CELL[(i * R) + j].fromR;
						CELL[(i * R) + j].toR = CELL[(i * R) + j].fromL;
					}
					CELL[(i * R) + j].sigToT = 0;
					CELL[(i * R) + j].sigToB = 0;
					CELL[(i * R) + j].sigToL = 0;
					CELL[(i * R) + j].sigToR = 0;
				}
			}

			/* If signal received, unload data for transfer */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					if (CELL[(i * R) + j].sigFromT > 0) {
						CELL[(i * R) + j].sigToB = CELL[(i * R) + j].sigFromT;
						CELL[(i * R) + j].sigFromT = 0;
						CELL[(i * R) + j].toL = CELL[(i * R) + j].val;
						CELL[(i * R) + j].toR = CELL[(i * R) + j].val;
					}
					if (CELL[(i * R) + j].sigFromB > 0) {
						CELL[(i * R) + j].sigToT = CELL[(i * R) + j].sigFromB;
						CELL[(i * R) + j].sigFromB = 0;
						CELL[(i * R) + j].toL = CELL[(i * R) + j].val;
						CELL[(i * R) + j].toR = CELL[(i * R) + j].val;
					}
					if (CELL[(i * R) + j].sigFromL > 0) {
						CELL[(i * R) + j].sigToR = CELL[(i * R) + j].sigFromL;
						CELL[(i * R) + j].sigFromL = 0;
						CELL[(i * R) + j].toT = CELL[(i * R) + j].val;
						CELL[(i * R) + j].toB = CELL[(i * R) + j].val;
					}
					if (CELL[(i * R) + j].sigFromR > 0) {
						CELL[(i * R) + j].sigToL = CELL[(i * R) + j].sigFromR;
						CELL[(i * R) + j].sigFromR = 0;
						CELL[(i * R) + j].toT = CELL[(i * R) + j].val;
						CELL[(i * R) + j].toB = CELL[(i * R) + j].val;
					}
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
		end = MPI_Wtime();
		if (rank == 0) {
			printf("N: %d, P: %d, R: %d, 4N time: %.4fs\n", N, P, R, end - start);
		}
		/*
		for(i = 0; i < R; i++) {
			for(j = 0; j < R; j++) {
				if (CELL[(i * R) + j].val != D0[((i + baseRow) * N) + (j + baseCol)]) {
					printf("4N ERROR: rank %d, i: %d, j: %d\n", rank, i, j);
				
				}
			}
		}
		*/





		//=========================================================================
		//
		// 3.5N (?) Algorithm
		//
		//=========================================================================
		for(i = 0; i < R; i++) {
			for(j = 0; j < R; j++) {
				CELL[(i * R) + j].val = INF;
				CELL[(i * R) + j].toL = INF;
				CELL[(i * R) + j].toR = INF;
				CELL[(i * R) + j].toT = INF;
				CELL[(i * R) + j].toB = INF;
				CELL[(i * R) + j].fromL = INF;
				CELL[(i * R) + j].fromR = INF;
				CELL[(i * R) + j].fromT = INF;
				CELL[(i * R) + j].fromB = INF;
			}
		}

		start = MPI_Wtime();
		for (step = 1; step <= 3.5 * N; step++) {

			/* Send to Top */
			for (j = 0; j < R; j++) {
				txBuf[j] = CELL[j].toT;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, top, 1, fromB, R, MPI_DOUBLE, bottom, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[((R - 1) * R) + j].fromB = fromB[j];
			}
			/* Send to Bottom */
			for (j = 0; j < R; j++) {
				txBuf[j] = CELL[((R - 1) * R) + j].toB;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, bottom, 1, fromT, R, MPI_DOUBLE, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].fromT = fromT[j];
			}
			/* Send to Left */
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[i * R].toL;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, left, 1, fromR, R, MPI_DOUBLE, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].fromR = fromR[i];
			}
			/* Send to Right */
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[(i * R) + (R - 1)].toR;
			}
			MPI_Sendrecv(txBuf, R, MPI_DOUBLE, right, 1, fromL, R, MPI_DOUBLE, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].fromL = fromL[i];
			}

			/* Simulate transfer within R * R array */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					if (i > 0) {
						CELL[(i * R) + j].fromT = CELL[((i - 1) * R) + j].toB;
					}
					if (i < (R - 1)) {
						CELL[(i * R) + j].fromB = CELL[((i + 1) * R) + j].toT;
					}
					if (j > 0) {
						CELL[(i * R) + j].fromL = CELL[(i * R) + (j - 1)].toR;
					}
					if (j < (R - 1)) {
						CELL[(i * R) + j].fromR = CELL[(i * R) + (j + 1)].toL;
					}
				}
			}

			/* Perform the triple operation */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromT + CELL[(i * R) + j].fromL);
					CELL[(i * R) + j].val = min(CELL[(i * R) + j].val, CELL[(i * R) + j].fromB + CELL[(i * R) + j].fromR);
				}
			}

			/* Unload when necessary */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					if ((step % N) == ((i + baseRow + 1) + (2 * (j + baseCol + 1)) - 2) % N) {
						CELL[(i * R) + j].fromL = CELL[(i * R) + j].val;
					}
					if ((step % N) == ((2 * (i + baseRow + 1)) + (j + baseCol + 1) - 2) % N) {
						CELL[(i * R) + j].fromT = CELL[(i * R) + j].val;
					}
					if ((step % N) == ((3 * N) - (i + baseRow + 1) - (2 * (j + baseCol + 1)) + 1) % N) {
						CELL[(i * R) + j].fromR = CELL[(i * R) + j].val;
					}
					if ((step % N) == ((3 * N) - (2 * (i + baseRow + 1)) - (j + baseCol + 1) + 1) % N) {
						CELL[(i * R) + j].fromB = CELL[(i * R) + j].val;
					}
				}
			}

			/* Left Column - Load D from L */
			if ((rank % P) == 0) {
				for (i = 0; i < R; i++) {
					if (step <= N + (i + baseRow)) {
						idx = ((i + baseRow) * N) + (step - (i + baseRow) - 1);
						if (idx >= ((i + baseRow) * N)) {
							if (CELL[(i * R)].fromL > D[idx]) {
								CELL[(i * R)].fromL = D[idx];
							}
						}
					}
				}
			}
			/* Top Row - Load D from T */
			if (rank < P) {
				for (j = 0; j < R; j++) {
					if (step <= N + (j + baseCol)) {
						idx = ((step - (j + baseCol) - 1) * N) + (j + baseCol);
						if (idx >= (j + baseCol)) {
							if (CELL[j].fromT > D[idx]) {
								CELL[j].fromT = D[idx];
							}
						}
					}
				}
			}
			/* Right Column - Load D from R */
			if ((rank % P) == (P - 1)) {
				for (i = 0; i < R; i++) {
					if (step <= (2 * N) - (i + baseRow) - 1) {
						idx = ((i + baseRow) * N) + ((2 * N) - (i + baseRow) - step - 1);
						if (idx <= ((i + baseRow) * N) + (baseCol + R - 1)) {
							if (CELL[(i * R) + (R - 1)].fromR > D[idx]) {
								CELL[(i * R) + (R - 1)].fromR = D[idx];
							}
						}
					}
				}
			}
			/* Bottom Row - Load D from B */
			if (rank >= P * (P - 1)) {
				for (j = 0; j < R; j++) {
					if (step <= (2 * N) - (j + baseCol) - 1) {
						idx = (((2 * N) - (j + baseCol) - step - 1) * N) + (j + baseCol);
						if (idx <= ((N - 1) * N) + (baseCol + j)) {
							if (CELL[((R - 1) * R) + j].fromB > D[idx]) {
								CELL[((R - 1) * R) + j].fromB = D[idx];
							}
						}
					}
				}
			}

			/* Set the registers for the data transfer in the next step */
			for (i = 0; i < R; i++) {
				for (j = 0; j < R; j++) {
					CELL[(i * R) + j].toT = CELL[(i * R) + j].fromB;
					CELL[(i * R) + j].toB = CELL[(i * R) + j].fromT;
					CELL[(i * R) + j].toL = CELL[(i * R) + j].fromR;
					CELL[(i * R) + j].toR = CELL[(i * R) + j].fromL;
				}
			}
		}
		MPI_Barrier(MPI_COMM_WORLD);
		end = MPI_Wtime();
		if (rank == 0) {
			printf("N: %d, P: %d, R: %d, 3.5N time: %.4fs\n", N, P, R, end - start);
		}

		/*
		for(i = 0; i < R; i++) {
			for(j = 0; j < R; j++) {
				if (CELL[(i * R) + j].val < D0[((i + baseRow) * N) + (j + baseCol)] - ERR
				 || CELL[(i * R) + j].val > D0[((i + baseRow) * N) + (j + baseCol)] + ERR) {
					printf("3N ERROR: rank %d, i: %d, j: %d, correct Val: %.8f, actual Val: %.8f\n", rank, i, j, D0[((i + baseRow) * N) + (j + baseCol)], CELL[(i * R) + j].val);
				}
			}
		}
		*/
	}

	MPI_Finalize();
	return 0;
}


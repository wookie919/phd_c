#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define N 6
#define TRUE 1
#define FALSE 0

#define MAXINT 10

#define INF (MAXINT * N * N) + 1

struct cell {
	int maximum;
	int control;
	int ctrlFromL, ctrlToR, ctrlFromR, ctrlToL;
	int column;
	int colToB, colFromT;
	int top;
	int topToB, topFromT;
	int w, n;
	int wToR, nToL, wFromL, nFromR, wToB, nToB, wFromT, nFromT;
	int old_w, old_n;
	int sol;
	int solToB, solFromT, solToR, solFromL, solToL, solFromR;
	int *sto_w, *sto_n, *sto_c;
};
typedef struct cell cell;


int max(int c1, int c2) {
	if (c1 > c2) {
		return c1;
	}
	else {
		return c2;
	}
}


int main(int argc, char *argv[]) {

	MPI_Comm comm;
	MPI_Status status;

	int rank, size;
	int dims[2], periods[2], coords[2];
	int left, right, top, bottom;
	int P; // P * P is the total number of processors
	int R; // R * R is the number of cells that one processor is simulating
	int baseRow, baseCol;

	unsigned int seed;

	int i, j, k;
	int idx;
	int *A, *B, *COL;
	cell *CELL;
	int *txBuf;
	int *ctrlFromL, *ctrlFromR, *colFromT, *wFromL, *nFromR, *wFromT, *nFromT, *topFromT, *solFromT, *solFromL, *solFromR;
	int a, wn;
	cell *c;
	int cand1, cand2, cand3;
	int cellRank;
	int maximum;

	double start, end;

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	dims[0] = 0;
	dims[1] = 0;
	MPI_Dims_create(size, 2, dims);
	if(dims[0] != dims[1]) {
		if(rank == 0) {
			printf("The number of processors must be a square.\n");
		}
		MPI_Finalize();
		return 0;
	}
	P = dims[0];
	R = N / P;
	periods[0] = 1;
	periods[1] = 1;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &comm);
	MPI_Cart_shift(comm, 1, 1, &left, &right);
	MPI_Cart_shift(comm, 0, 1, &top, &bottom);
	MPI_Cart_coords(comm, rank, dims[0], coords);
	baseRow = R * coords[0];
	baseCol = R * coords[1];


	if (rank == 0) {
		seed = time(NULL);
		//seed = 1436833303;
		printf("seed: %d\n", seed);
		for (i = 1; i < P * P; i++) {
			MPI_Send(&seed, 1, MPI_INT, i, 1, comm);
		}
	}
	else {
		MPI_Recv(&seed, 1, MPI_INT, 0, 1, comm, &status);
	}
	srand(seed);

	A = (int*) malloc(N * N * sizeof(int));
	B = (int*) malloc(N * N * sizeof(int));
	COL = (int*) malloc(N * N * sizeof(int));
	for(i = 0; i < N; i++) {
		for(j = 0; j < N; j++) {
			//A[(i * N) + j] = rand() - (RAND_MAX / 2);
			A[(i * N) + j] = (rand() % MAXINT) - (MAXINT / 2);
		}
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (i == 0) {
				COL[j] = A[j];
			}
			else {
				COL[(i * N) + j] = COL[((i - 1) * N) + j] + A[(i * N) + j];
			}
		}
	}

	if (rank == 0) {
		for(i = 0; i < N; i++) {
			for(j = 0; j < N; j++) {
				printf("%3d ", A[(i * N) + j]);
			}
			printf("\n");
		}
		printf("================================\n");
	}

	txBuf = (int*) malloc(R * sizeof(int));
	ctrlFromL = (int*) malloc(R * sizeof(int));
	ctrlFromR = (int*) malloc(R * sizeof(int));
	colFromT = (int*) malloc(R * sizeof(int));
	wFromL = (int*) malloc(R * sizeof(int));
	nFromR = (int*) malloc(R * sizeof(int));
	wFromT = (int*) malloc(R * sizeof(int));
	nFromT = (int*) malloc(R * sizeof(int));
	topFromT = (int*) malloc(R * sizeof(int));
	solFromT = (int*) malloc(R * sizeof(int));
	solFromL = (int*) malloc(R * sizeof(int));
	solFromR = (int*) malloc(R * sizeof(int));

	// Allocate memory for each cell.
	CELL = (cell*) malloc(R * R * sizeof(cell));
	for (i = 0; i < R; i++) {
		for (j = 0; j < R; j++) {
			c = &(CELL[(i * R) + j]);
			c->sto_c = (int*) malloc(N * sizeof(int));
			c->sto_w = (int*) malloc(N * sizeof(int));
			c->sto_n = (int*) malloc(N * sizeof(int));
			for (k = 0; k < N; k++) {
				c->sto_w[k] = -INF;
				c->sto_n[k] = -INF;
				c->sto_c[k] = -INF;
			}
		}
	}





	start = MPI_Wtime();





	/***************************************************************************
	 *
	 * Compute W
	 *
	 **************************************************************************/
	for (i = 0; i < R; i++) {
		for (j = 0; j < R; j++) {
			a = A[((i + baseRow) * N) + (j + baseCol)];
			CELL[(i * R) + j].control = FALSE;
			CELL[(i * R) + j].ctrlToR = FALSE;
			CELL[(i * R) + j].ctrlFromL = FALSE;
			CELL[(i * R) + j].column = 0;
			CELL[(i * R) + j].colToB = 0;
			CELL[(i * R) + j].colFromT = 0;
			CELL[(i * R) + j].top = a;
			CELL[(i * R) + j].topToB = a;
			CELL[(i * R) + j].topFromT = 0;
			CELL[(i * R) + j].w = 0;
			CELL[(i * R) + j].wToR = 0;
			CELL[(i * R) + j].wFromL = 0;
			CELL[(i * R) + j].wToB = 0;
			CELL[(i * R) + j].wFromT = 0;
			CELL[(i * R) + j].old_w = 0;
			CELL[(i * R) + j].sol = 0;
			CELL[(i * R) + j].solToB = 0;
			CELL[(i * R) + j].solFromT = 0;
			CELL[(i * R) + j].solToR = 0;
			CELL[(i * R) + j].solFromL = 0;
			
		}
	}
	for (i = 0; i < R; i++) {
		CELL[(i * R)].ctrlFromL = TRUE;
		CELL[(i * R)].solFromL = -INF;
	}
	for (j = 0; j < R; j++) {
		CELL[j].solFromT = -INF;
	}

	// Main loop for W
	for (k = 1; k <= 2 * N - 1; k++) {
		// Take care of top to bottom transfers between actual processors.
		if (rank < P) { // If TOP row
			for (j = 0; j < R; j++) { /* Send "column" down */
				txBuf[j] = CELL[((R - 1) * R) + j].colToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "w" down */
				txBuf[j] = CELL[((R - 1) * R) + j].wToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "top" down */
				txBuf[j] = CELL[((R - 1) * R) + j].topToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "sol" down */
				txBuf[j] = CELL[((R - 1) * R) + j].solToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
		}
		else if (rank >= P * (P - 1)) { // If BOTTOM row
			MPI_Recv(colFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "column" from top */
			for (j = 0; j < R; j++) {
				CELL[j].colFromT = colFromT[j];
			}
			MPI_Recv(wFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "w" from top */
			for (j = 0; j < R; j++) {
				CELL[j].wFromT = wFromT[j];
			}
			MPI_Recv(topFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "top" from top */
			for (j = 0; j < R; j++) {
				CELL[j].topFromT = topFromT[j];
			}
			MPI_Recv(solFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "sol" from top */
			for (j = 0; j < R; j++) {
				CELL[j].solFromT = solFromT[j];
			}
		}
		else {
			for (j = 0; j < R; j++) { /* Sendrecv "column" */
				txBuf[j] = CELL[((R - 1) * R) + j].colToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, colFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].colFromT = colFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "w" */
				txBuf[j] = CELL[((R - 1) * R) + j].wToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, wFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].wFromT = wFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "top" */
				txBuf[j] = CELL[((R - 1) * R) + j].topToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, topFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].topFromT = topFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "sol" */
				txBuf[j] = CELL[((R - 1) * R) + j].solToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, solFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].solFromT = solFromT[j];
			}
		}

		// Take care of left to right transfers between actual processors.
		if ((rank % P) == 0) { // If left most column
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[(i * R) + (R - 1)].ctrlToR;
			}
			MPI_Send(txBuf, R, MPI_INT, right, 1, comm);
			for (i = 0; i < R; i++) { /* Send "w" right */
				txBuf[i] = CELL[(i * R) + (R - 1)].wToR;
			}
			MPI_Send(txBuf, R, MPI_INT, right, 1, comm);
			for (i = 0; i < R; i++) { /* Send "sol" right */
				txBuf[i] = CELL[(i * R) + (R - 1)].solToR;
			}
			MPI_Send(txBuf, R, MPI_INT, right, 1, comm);
		}
		else if ((rank % P) == (P - 1)) { // If right most column
			MPI_Recv(ctrlFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].ctrlFromL = ctrlFromL[i];
			}
			MPI_Recv(wFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].wFromL = wFromL[i];
			}
			MPI_Recv(solFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].solFromL = solFromL[i];
			}
		}
		else {
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[(i * R) + (R - 1)].ctrlToR;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, right, 1, ctrlFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].ctrlFromL = ctrlFromL[i];
			}
			for (i = 0; i < R; i++) { /* Send "w" right */
				txBuf[i] = CELL[(i * R) + (R - 1)].wToR;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, right, 1, wFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].wFromL = wFromL[i];
			}
			for (i = 0; i < R; i++) { /* Send "sol" right */
				txBuf[i] = CELL[(i * R) + (R - 1)].solToR;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, right, 1, solFromL, R, MPI_INT, left, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[i * R].solFromL = solFromL[i];
			}
		}

		/* Simulate transfer within each processor R * R array */
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				if (i > 0) {
					CELL[(i * R) + j].colFromT = CELL[((i - 1) * R) + j].colToB;
					CELL[(i * R) + j].wFromT = CELL[((i - 1) * R) + j].wToB;
					CELL[(i * R) + j].topFromT = CELL[((i - 1) * R) + j].topToB;
					CELL[(i * R) + j].solFromT = CELL[((i - 1) * R) + j].solToB;
				}
				if (j > 0) {
					CELL[(i * R) + j].ctrlFromL = CELL[(i * R) + (j - 1)].ctrlToR;
					CELL[(i * R) + j].solFromL = CELL[(i * R) + (j - 1)].solToR;
					CELL[(i * R) + j].wFromL = CELL[(i * R) + (j - 1)].wToR;
				}
			}
		}

		/*********************************
		 * Perform the actual computation
		 *********************************/
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				c = &(CELL[(i * R) + j]);
				a = A[((i + baseRow) * N) + (j + baseCol)];
				if (c->ctrlFromL) {
					c->column = c->colFromT + a;
					cand1 = c->wFromL + c->column;
					cand2 = c->wFromT + a;
					cand3 = c->old_w + c->top;
					c->w = max(max(cand1, cand2), cand3);
					idx = (i + baseRow) + (j + baseCol) - k + 1;
					if (idx >= 0 && idx < N) {
						c->sto_w[idx] = c->w;
						c->sto_c[idx] = c->column;
					}
					c->old_w = c->w;
					c->top = c->topFromT;
					c->sol = max(max(max(c->solFromT, c->solFromL), c->sol), c->w);
					c->control = TRUE;
				}
				else {
					c->control = c->ctrlFromL;
					c->column = c->colFromT;
				}
			}
		}

		/* Set the registers for the data transfer in the next step */
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				CELL[(i * R) + j].ctrlToR = CELL[(i * R) + j].control;
				CELL[(i * R) + j].colToB = CELL[(i * R) + j].column;
				CELL[(i * R) + j].topToB = CELL[(i * R) + j].top;
				CELL[(i * R) + j].wToR = CELL[(i * R) + j].w;
				CELL[(i * R) + j].wToB = CELL[(i * R) + j].w;
				CELL[(i * R) + j].solToR = CELL[(i * R) + j].sol;
				CELL[(i * R) + j].solToB = CELL[(i * R) + j].sol;
			}
		}
	}




	/***************************************************************************
	 *
	 * Compute N
	 *
	 **************************************************************************/
	for (i = 0; i < R; i++) {
		for (j = 0; j < R; j++) {
			a = A[((i + baseRow) * N) + (j + baseCol)];
			CELL[(i * R) + j].control = FALSE;
			CELL[(i * R) + j].ctrlToL = FALSE;
			CELL[(i * R) + j].ctrlFromR = FALSE;
			CELL[(i * R) + j].column = 0;
			CELL[(i * R) + j].colToB = 0;
			CELL[(i * R) + j].colFromT = 0;
			CELL[(i * R) + j].top = a;
			CELL[(i * R) + j].topToB = a;
			CELL[(i * R) + j].topFromT = 0;
			CELL[(i * R) + j].n = 0;
			CELL[(i * R) + j].nToL = 0;
			CELL[(i * R) + j].nFromR = 0;
			CELL[(i * R) + j].nToB = 0;
			CELL[(i * R) + j].nFromT = 0;
			CELL[(i * R) + j].old_n = 0;
			CELL[(i * R) + j].sol = 0;
			CELL[(i * R) + j].solToB = 0;
			CELL[(i * R) + j].solFromT = 0;
			CELL[(i * R) + j].solToL = 0;
			CELL[(i * R) + j].solFromR = 0;
		}
	}
	for (i = 0; i < R; i++) {
		CELL[(i * R) + (R - 1)].ctrlFromR = TRUE;
		CELL[(i * R) + (R - 1)].solFromR = -INF;
	}
	for (j = 0; j < R; j++) {
		CELL[j].solFromT = -INF;
	}

	// Main loop for W
	for (k = 1; k <= 2 * N - 1; k++) {

		// Take care of top to bottom transfers between actual processors.
		if (rank < P) { // If TOP row
			for (j = 0; j < R; j++) { /* Send "column" down */
				txBuf[j] = CELL[((R - 1) * R) + j].colToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "w" down */
				txBuf[j] = CELL[((R - 1) * R) + j].nToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "top" down */
				txBuf[j] = CELL[((R - 1) * R) + j].topToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
			for (j = 0; j < R; j++) { /* Send "sol" down */
				txBuf[j] = CELL[((R - 1) * R) + j].solToB;
			}
			MPI_Send(txBuf, R, MPI_INT, bottom, 1, comm);
		}
		else if (rank >= P * (P - 1)) { // If BOTTOM row
			MPI_Recv(colFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "column" from top */
			for (j = 0; j < R; j++) {
				CELL[j].colFromT = colFromT[j];
			}
			MPI_Recv(nFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "w" from top */
			for (j = 0; j < R; j++) {
				CELL[j].nFromT = nFromT[j];
			}
			MPI_Recv(topFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "top" from top */
			for (j = 0; j < R; j++) {
				CELL[j].topFromT = topFromT[j];
			}
			MPI_Recv(solFromT, R, MPI_INT, top, 1, comm, &status); /* Receive "sol" from top */
			for (j = 0; j < R; j++) {
				CELL[j].solFromT = solFromT[j];
			}
		}
		else {
			for (j = 0; j < R; j++) { /* Sendrecv "column" */
				txBuf[j] = CELL[((R - 1) * R) + j].colToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, colFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].colFromT = colFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "n" */
				txBuf[j] = CELL[((R - 1) * R) + j].nToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, nFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].nFromT = nFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "top" */
				txBuf[j] = CELL[((R - 1) * R) + j].topToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, topFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].topFromT = topFromT[j];
			}
			for (j = 0; j < R; j++) { /* Sendrecv "sol" */
				txBuf[j] = CELL[((R - 1) * R) + j].solToB;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, bottom, 1, solFromT, R, MPI_INT, top, 1, comm, &status);
			for (j = 0; j < R; j++) {
				CELL[j].solFromT = solFromT[j];
			}
		}

		// Take care of right to left transfers between actual processors.
		if ((rank % P) == P - 1) { // If right most column
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[i * R].ctrlToL;
			}
			MPI_Send(txBuf, R, MPI_INT, left, 1, comm);
			for (i = 0; i < R; i++) { /* Send "w" right */
				txBuf[i] = CELL[i * R].nToL;
			}
			MPI_Send(txBuf, R, MPI_INT, left, 1, comm);
			for (i = 0; i < R; i++) { /* Send "sol" right */
				txBuf[i] = CELL[i * R].solToL;
			}
			MPI_Send(txBuf, R, MPI_INT, left, 1, comm);
		}
		else if ((rank % P) == 0) { // If left most column
			MPI_Recv(ctrlFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].ctrlFromR = ctrlFromR[i];
			}
			MPI_Recv(nFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].nFromR = nFromR[i];
			}
			MPI_Recv(solFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].solFromR = solFromR[i];
			}
		}
		else {
			for (i = 0; i < R; i++) {
				txBuf[i] = CELL[(i * R)].ctrlToL;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, left, 1, ctrlFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].ctrlFromR = ctrlFromR[i];
			}
			for (i = 0; i < R; i++) { /* Send "n" left */
				txBuf[i] = CELL[(i * R)].nToL;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, left, 1, nFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].nFromR = nFromR[i];
			}
			for (i = 0; i < R; i++) { /* Send "sol" left */
				txBuf[i] = CELL[(i * R)].solToL;
			}
			MPI_Sendrecv(txBuf, R, MPI_INT, left, 1, solFromR, R, MPI_INT, right, 1, comm, &status);
			for (i = 0; i < R; i++) {
				CELL[(i * R) + (R - 1)].solFromR = solFromR[i];
			}
		}

		/* Simulate transfer within each processor R * R array */
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				if (i > 0) {
					CELL[(i * R) + j].colFromT = CELL[((i - 1) * R) + j].colToB;
					CELL[(i * R) + j].nFromT = CELL[((i - 1) * R) + j].nToB;
					CELL[(i * R) + j].topFromT = CELL[((i - 1) * R) + j].topToB;
					CELL[(i * R) + j].solFromT = CELL[((i - 1) * R) + j].solToB;
				}
				if (j < R - 1) {
					CELL[(i * R) + j].ctrlFromR = CELL[(i * R) + (j + 1)].ctrlToL;
					CELL[(i * R) + j].solFromR = CELL[(i * R) + (j + 1)].solToL;
					CELL[(i * R) + j].nFromR = CELL[(i * R) + (j + 1)].nToL;
				}
			}
		}

		/*********************************
		 * Perform the actual computation
		 *********************************/
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				c = &(CELL[(i * R) + j]);
				a = A[((i + baseRow) * N) + (j + baseCol)];
				if (c->ctrlFromR) {
					c->column = c->colFromT + a;
					cand1 = c->nFromR + c->column;
					cand2 = c->nFromT + a;
					cand3 = c->old_n + c->top;
					c->n = max(max(cand1, cand2), cand3);
					idx = (i + baseRow) + N - (j + baseCol) - k;
					if (idx >= 0 && idx < N) {
						c->sto_n[idx] = c->n;
					}
					c->old_n = c->n;
					c->top = c->topFromT;
					c->sol = max(max(max(c->solFromT, c->solFromR), c->sol), c->n);
					c->control = TRUE;
				}
				else {
					c->control = c->ctrlFromR;
					c->column = c->colFromT;
				}
			}
		}

		/* Set the registers for the data transfer in the next step */
		for (i = 0; i < R; i++) {
			for (j = 0; j < R; j++) {
				CELL[(i * R) + j].ctrlToL = CELL[(i * R) + j].control;
				CELL[(i * R) + j].colToB = CELL[(i * R) + j].column;
				CELL[(i * R) + j].topToB = CELL[(i * R) + j].top;
				CELL[(i * R) + j].nToL = CELL[(i * R) + j].n;
				CELL[(i * R) + j].nToB = CELL[(i * R) + j].n;
				CELL[(i * R) + j].solToL = CELL[(i * R) + j].sol;
				CELL[(i * R) + j].solToB = CELL[(i * R) + j].sol;
			}
		}
	}

	MPI_Barrier(MPI_COMM_WORLD);





	/***************************************************************************
	 *
	 * Combine W and N
	 *
	 **************************************************************************/
	maximum = -INF;
	for (i = 0; i < R; i++) {
		for (j = 0; j < R; j++) {
			c = &(CELL[(i * R) + j]);
			c->maximum = -INF;
			for (k = 0; k < N; k++) {
				wn = c->sto_w[k] + c->sto_n[k] - c->sto_c[k];
				if (wn > c->maximum) {
					c->maximum = wn;
				}
			}
			if (c->maximum > maximum) {
				maximum = c->maximum;
			}
		}
	}

	end = MPI_Wtime();



	printf("rank %d: %d\n", rank, maximum);
	MPI_Barrier(MPI_COMM_WORLD);


	if (rank == 0) {
		printf("N: %d, P: %d, R: %d, time: %.4fs\n", N, P, R, end - start);
	}

	MPI_Finalize();

	return 0;
}


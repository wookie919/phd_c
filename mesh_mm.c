#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

#define TRUE 1
#define FALSE 0

#define DEBUG TRUE


void reverse_array(int *array, int n) {
	int left, right;
	int temp;
	
	left = 0;
	right = n - 1;
	
	while (left < right) {
		temp = array[left];
		array[left] = array[right];
		array[right] = temp;
		left++;
		right--;
	}
}


int main(int argc, char ** argv) {

	int i, j, k;
	int N, NS;
	int step, rank;

	int RANK, SIZE;

	int *tmpIntPtr; /* Used for memory allocation of matrices */
	int **A, **B, **C, **D; /* N x N matrices, C = A x B, D = A x B */

	int seed;
	
	int *txBuf;
	int MASTER_RANK;
	int LOADER_L_BASE_RANK;
	int LOADER_R_BASE_RANK;
	int LOADER_T_BASE_RANK;
	int LOADER_B_BASE_RANK;
	
	int *ROW, *COL;
	int fromL, fromR, fromT, fromB;
	int toL, toR, toT, toB;
	int cellValue;
	int cellRow, cellCol;
	int rowT, rowB, colL, colR;
	int rankT, rankB, rankL, rankR, rankLoader;
	int index;
	
	int NUM_STEPS;
	
	MPI_Status status;
	
	
	/* Check parameters and initialize all processes */
	if (argc != 2) {
		printf("Usage: %s <n>\n", argv[0]);
		return 0;
	}
	N = atoi(argv[1]);
	NS = N * N;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	
	MASTER_RANK = SIZE - 1;

	if ((NS + (4 * N) + 1) != SIZE) {
		if (RANK == MASTER_RANK) {
			printf("Number of processors must be n^2 + 4n + 1\n");
		}
		MPI_Finalize();
		return 0;
	}

	LOADER_L_BASE_RANK = NS;
	LOADER_R_BASE_RANK = NS + N;
	LOADER_T_BASE_RANK = NS + N + N;
	LOADER_B_BASE_RANK = NS + N + N + N;

	NUM_STEPS = N + N/2 - 1;

	/***************************************************************************
	 *
	 * Code for MASTER
	 *
	 **************************************************************************/
	if (RANK == MASTER_RANK) {
		seed = time(NULL);
		if (DEBUG) printf("Seed: %d\n", seed);
		srand(seed);
	
		/* Initialize matrices */
		A = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NS * sizeof(int));
		for (i = 0; i < N; i++) A[i] = tmpIntPtr + (i * N);
		B = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NS * sizeof(int));
		for (i = 0; i < N; i++) B[i] = tmpIntPtr + (i * N);
		C = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NS * sizeof(int));
		for (i = 0; i < N; i++) C[i] = tmpIntPtr + (i * N);
		D = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NS * sizeof(int));
		for (i = 0; i < N; i++) D[i] = tmpIntPtr + (i * N);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				A[i][j] = rand() % N;
				B[i][j] = rand() % N;
				C[i][j] = 0;
				D[i][j] = 0;
			}
		}
		
		if (DEBUG) {
			printf("======== A ========\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", A[i][j]);
				}
				printf("\n");
			}
			printf("======== B ========\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", B[i][j]);
				}
				printf("\n");
			}
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					for (k = 0; k < N; k++) {
						D[i][j] = D[i][j] + A[i][k] * B[k][j];
					}
				}
			}
			printf("======== D ========\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", D[i][j]);
				}
				printf("\n");
			}
		}

		/* Transmit rows and columns to loaders. */
		txBuf = (int*) malloc(N * sizeof(int));
		
		/* Send rows of A to LEFT and RIGHT LOADERS */
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				txBuf[j] = A[i][j];
			}
			MPI_Send(&(txBuf[0]), N, MPI_INT, LOADER_L_BASE_RANK + i, 0, MPI_COMM_WORLD);
			reverse_array(txBuf, N);
			MPI_Send(&(txBuf[0]), N, MPI_INT, LOADER_R_BASE_RANK + i, 0, MPI_COMM_WORLD);
		}
		
		/* Send columns of B to TOP and BOTTOL LOADERS */
		for (j = 0; j < N; j++) {
			for (i = 0; i < N; i++) {
				txBuf[i] = B[i][j];
			}
			MPI_Send(&(txBuf[0]), N, MPI_INT, LOADER_T_BASE_RANK + j, 0, MPI_COMM_WORLD);
			reverse_array(txBuf, N);
			MPI_Send(&(txBuf[0]), N, MPI_INT, LOADER_B_BASE_RANK + j, 0, MPI_COMM_WORLD);
		}

		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */
		
		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
		
			if (DEBUG) {
				for (i = 0; i < N; i++) {
					for (j = 0; j < N; j++) {
						rank = (i * N) + j;
						MPI_Recv(&(C[i][j]), 1, MPI_INT, rank, step, MPI_COMM_WORLD, &status);
					}
				}
				printf("MASTER: after step %d:\n", step);
				for (i = 0; i < N; i++) {
					for (j = 0; j < N; j++) {
						printf("%d ", C[i][j]);
					}
					printf("\n");
				}
			}

			MPI_Barrier(MPI_COMM_WORLD);
		}
		
		if (DEBUG) {
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					if (C[i][j] != D[i][j]) {
						printf("!!! ERROR !!!: C[%d][%d] = %d, D[%d][%d] = %d\n", i, j, C[i][j], i, j, D[i][j]);
					}
				}
			}
		}
	}
	
	/***************************************************************************
	 *
	 * Code for Cells (There are NS = N^2 Cells)
	 *
	 **************************************************************************/
	else if (RANK >= 0 && RANK < NS) {
		cellValue = 0;
		fromL = 0;
		fromR = 0;
		fromT = 0;
		fromB = 0;
		toL = 0;
		toR = 0;
		toT = 0;
		toB = 0;

		/* deterine ranks of neighbours */
		cellRow = RANK / N;
		cellCol = RANK % N;
		rowT = cellRow - 1;
		rowB = cellRow + 1;
		colL = cellCol - 1;
		colR = cellCol + 1;
		if (rowT < 0) rowT = N - 1;
		if (rowB == N) rowB = 0;
		if (colL < 0) colL = N - 1;
		if (colR == N) colR = 0;
		rankT = rowT * N + cellCol;
		rankB = rowB * N + cellCol;
		rankL = cellRow * N + colL;
		rankR = cellRow * N + colR;

		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */
		
		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
			
			/* prepare the send/recv by copying FROM to TO */
			toL = fromR;
			toR = fromL;
			toT = fromB;
			toB = fromT;
			
			/* Send to R, Receive from L. */
			if (rankL > RANK) {
				rankLoader = LOADER_L_BASE_RANK + cellRow;
				MPI_Sendrecv(&toR, 1, MPI_INT, rankR, step, &fromL, 1, MPI_INT, rankLoader, step, MPI_COMM_WORLD, &status);
			}
			else if (rankR < RANK) {
				MPI_Recv(&fromL, 1, MPI_INT, rankL, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&toR, 1, MPI_INT, rankR, step, &fromL, 1, MPI_INT, rankL, step, MPI_COMM_WORLD, &status);
			}

			/* Send to L, Receive from R. */
			if (rankL > RANK) {
				MPI_Recv(&fromR, 1, MPI_INT, rankR, step, MPI_COMM_WORLD, &status);
			}
			else if (rankR < RANK) {
				rankLoader = LOADER_R_BASE_RANK + cellRow;
				MPI_Sendrecv(&toL, 1, MPI_INT, rankL, step, &fromR, 1, MPI_INT, rankLoader, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&toL, 1, MPI_INT, rankL, step, &fromR, 1, MPI_INT, rankR, step, MPI_COMM_WORLD, &status);
			}

			/* Send to T, Receive from B. */
			if (rankT > RANK) {
				MPI_Recv(&fromB, 1, MPI_INT, rankB, step, MPI_COMM_WORLD, &status);
			}
			else if (rankB < RANK) {
				rankLoader = LOADER_B_BASE_RANK + cellCol;
				MPI_Sendrecv(&toT, 1, MPI_INT, rankT, step, &fromB, 1, MPI_INT, rankLoader, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&toT, 1, MPI_INT, rankT, step, &fromB, 1, MPI_INT, rankB, step, MPI_COMM_WORLD, &status);
			}
			
			/* Send to B, Receive from T. */
			if (rankT > RANK) {
				rankLoader = LOADER_T_BASE_RANK + cellCol;
				MPI_Sendrecv(&toB, 1, MPI_INT, rankB, step, &fromT, 1, MPI_INT, rankLoader, step, MPI_COMM_WORLD, &status);
			}
			else if (rankB < RANK) {
				MPI_Recv(&fromT, 1, MPI_INT, rankT, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&toB, 1, MPI_INT, rankB, step, &fromT, 1, MPI_INT, rankT, step, MPI_COMM_WORLD, &status);
			}
			
			cellValue = cellValue + (fromL * fromT);
			cellValue = cellValue + (fromR * fromB);
			
			if (DEBUG) MPI_Send(&cellValue, 1, MPI_INT, MASTER_RANK, step, MPI_COMM_WORLD);

			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
	
	/***************************************************************************
	 *
	 * Code for LEFT LOADER (There are N LOADER_L)
	 *
	 **************************************************************************/
	else if (RANK >= LOADER_L_BASE_RANK && RANK < LOADER_L_BASE_RANK + N) {
		ROW = (int*) malloc(N * sizeof(int));
		MPI_Recv(&(ROW[0]), N, MPI_INT, MASTER_RANK, 0, MPI_COMM_WORLD, &status);
		
		index = -1 - (RANK - LOADER_L_BASE_RANK);
		rankR = (RANK - LOADER_L_BASE_RANK) * N;
		
		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */
		
		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
			index++;
			toR = 0;
			if (index >= 0 && index < N) {
				toR = ROW[index];
			}
			MPI_Send(&toR, 1, MPI_INT, rankR, step, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
	
	/***************************************************************************
	 *
	 * Code for RIGHT LOADER (There are N LOADER_R)
	 *
	 **************************************************************************/
	else if (RANK >= LOADER_R_BASE_RANK && RANK < LOADER_R_BASE_RANK + N) {
		ROW = (int*) malloc(N * sizeof(int));
		MPI_Recv(&(ROW[0]), N, MPI_INT, MASTER_RANK, 0, MPI_COMM_WORLD, &status);
		
		index = (RANK - LOADER_R_BASE_RANK) - N;
		rankL = ((RANK - LOADER_R_BASE_RANK) * N) + N - 1;
		
		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */

		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
			index++;
			toL = 0;
			if (index >= 0 && index < N) {
				toL = ROW[index];
			}
			MPI_Send(&toL, 1, MPI_INT, rankL, step, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
	
	/***************************************************************************
	 *
	 * Code for TOP LOADER (There are N LOADER_T)
	 *
	 **************************************************************************/
	else if (RANK >= LOADER_T_BASE_RANK && RANK < LOADER_T_BASE_RANK + N) {
		COL = (int*) malloc(N * sizeof(int));
		MPI_Recv(&(COL[0]), N, MPI_INT, MASTER_RANK, 0, MPI_COMM_WORLD, &status);
		
		index = -1 - (RANK - LOADER_T_BASE_RANK);
		rankB = (RANK - LOADER_T_BASE_RANK);
		
		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */
		
		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
			index++;
			toB = 0;
			if (index >= 0 && index < N) {
				toB = COL[index];
			}
			MPI_Send(&toB, 1, MPI_INT, rankB, step, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
	
	/***************************************************************************
	 *
	 * Code for BOTTOM LOADER (There are N LOADER_B)
	 *
	 **************************************************************************/
	else if (RANK >= LOADER_B_BASE_RANK && RANK < LOADER_B_BASE_RANK + N) {
		COL = (int*) malloc(N * sizeof(int));
		MPI_Recv(&(COL[0]), N, MPI_INT, MASTER_RANK, 0, MPI_COMM_WORLD, &status);
		
		index = (RANK - LOADER_B_BASE_RANK) - N;
		rankT = (N * (N - 1)) + (RANK - LOADER_B_BASE_RANK);
		
		MPI_Barrier(MPI_COMM_WORLD); /* Loaders are now ready */
		
		/******** MAIN LOOP ********/
		for (step = 0; step < NUM_STEPS; step++) {
			index++;
			toT = 0;
			if (index >= 0 && index < N) {
				toT = COL[index];
			}
			MPI_Send(&toT, 1, MPI_INT, rankT, step, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
		}
	}
	
	MPI_Finalize();
	
	return 0;
}



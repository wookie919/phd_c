#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "mpi.h"

#define TRUE 1
#define FALSE 0

#define DEBUG TRUE

int serial(int** A, int N) {

	int i, j, k;
	int NN;
	int SUM = -2 * N;
	int *tmpIntPtr;
	int **minSumA, **recSumA, **solSumA, **colSumA;
	int maxSum;

	NN = N * N;

	minSumA = (int**) malloc(N * sizeof(int*));
	recSumA = (int**) malloc(N * sizeof(int*));
	solSumA = (int**) malloc(N * sizeof(int*));
	colSumA = (int**) malloc(N * sizeof(int*));
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		minSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		recSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		solSumA[i] = tmpIntPtr + (i * N);
	}
	tmpIntPtr = (int*) malloc(NN * sizeof(int));
	for (i = 0; i < N; i++) {
		colSumA[i] = tmpIntPtr + (i * N);
	}
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			minSumA[i][j] = 0;
			recSumA[i][j] = 0;
			solSumA[i][j] = (-2 * N);
			colSumA[i][j] = 0;
		}
	}

	for (k = 0; k < N; k++) {
		if (k > 0) {
			for (j = 0; j < N; j++) {
				colSumA[k - 1][j] = 0;
			}
		}
		for (i = k; i < N; i++) {
			for (j = 0; j < N; j++) {
				if (i == 0) {
					colSumA[i][j] = A[i][j];
				}
				else {
					colSumA[i][j] = colSumA[i - 1][j] + A[i][j];
				}
				if (j == 0) {
					recSumA[i][j] = colSumA[i][j];
				}
				else {
					recSumA[i][j] = recSumA[i][j - 1] + colSumA[i][j];
				}
				if (j == 0) {
					if (recSumA[i][j] < 0) {
						minSumA[i][j] = recSumA[i][j];
					}
					else {
						minSumA[i][j] = 0;
					}
				}
				else {
					if (recSumA[i][j] < minSumA[i][j - 1]) {
						minSumA[i][j] = recSumA[i][j];
					}
					else {
						minSumA[i][j] = minSumA[i][j - 1];
					}
				}
				maxSum = recSumA[i][j] - minSumA[i][j];
				if (j == 0){
					solSumA[i][j] = maxSum;
				}
				else {
					if (maxSum > solSumA[i][j - 1]) {
						solSumA[i][j] = maxSum;
					}
					else {
						solSumA[i][j] = solSumA[i][j - 1];
					}
				}
			}
			if (solSumA[i][N - 1] > SUM) {
				SUM = solSumA[i][N - 1];
			}
		}
	}

	return SUM;
}


int main(int argc, char ** argv) {

	int i, j, k;
	int N, NN;
	int SUM_SERIAL;
	int SUM_PARALLEL;

	int SIZE, MASTER;
	int RANK, ROW, COL;
	int RANK_T, RANK_B, RANK_L, RANK_R;

	char TOP_EDGE = FALSE;
	char BOTTOM_EDGE = FALSE;
	char LEFT_EDGE = FALSE;
	char RIGHT_EDGE = FALSE;
	char LHS = FALSE;
	char RHS = FALSE;

	int *tmpIntPtr; /* Used for memory allocation of matrices */
	int **A; /* 2D array */

	int seed;
	int cellRank;
	int A_VAL;

	int colSum = 0, colSumT = 0, colSumB = 0;
	int solSum = 0;
	int recSumT = 0, recSumB = 0;
	int recSumUE = 0, recSumUO = 0;
	int minSumE = 0, minSumO = 0;
	int midSum = 0, midSumE = 0, midSumO = 0;
	int maxSum = 0, maxSumE = 0, maxSumO = 0;

	int colSumTFromT = 0, colSumBFromB = 0;
	int recSumTFromL = 0, recSumBFromL = 0;
	int recSumTFromR = 0, recSumBFromR = 0;
	int recSumBFromB = 0;
	int recSumBPrev = 0, recSumBPrevFromB = 0;
	int solSumFromT = 0, solSumFromB = 0;
	int midSumFromT = 0, midSumFromB = 0;
	int minSumEFromL = 0, minSumOFromL = 0;
	int solSumFromL = 0, solSumFromR = 0;
	int minSumEFromR = 0, minSumOFromR = 0;
	int maxSumFromR = 0, maxSumFromB = 0;
	int maxSumEFromR = 0, maxSumOFromR = 0;

	int step;

	int bottomMidRankL, bottomMidRankR;

	int maxSumCandidate1, maxSumCandidate2, maxSumCandidate3;
	MPI_Status status;

	clock_t startTime, endTime;
	double timeTaken;

	/* Check parameters and initialize all processes */
	/*
	if (argc != 2) {
		printf("Usage: %s <n>\n", argv[0]);
		return 0;
	}
	N = atoi(argv[1]);
	*/
	
	N = 8;
	
	NN = N * N;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &RANK);
	MPI_Comm_size(MPI_COMM_WORLD, &SIZE);
	if ((NN + 1) != SIZE) {
		if (RANK == MASTER) {
			printf("Number of processors must be n^2 + 1\n");
		}
		return 0;
	}
	MASTER = SIZE - 1;

	if (RANK == MASTER) {
		/***********************************************************************
		 *
		 * Code for MASTER
		 *
		 **********************************************************************/

		/* Initialize the 2D array */
		A = (int**) malloc(N * sizeof(int*));
		tmpIntPtr = (int*) malloc(NN * sizeof(int));
		for (i = 0; i < N; i++) {
			A[i] = tmpIntPtr + (i * N);
		}
		
		seed = time(NULL);
		//seed = 1400478255;
		srand(seed);

		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				A[i][j] = (rand() % (2 * N)) - N;
			}
		}
	
		if (DEBUG) {
			printf("======== A ========\n");
			for (i = 0; i < N; i++) {
				for (j = 0; j < N; j++) {
					printf("%d ", A[i][j]);
				}
				printf("\n");
			}
		}

		/* Transmit each A[i][j] to each cell */
		for (i = 0; i < N; i++) {
			for (j = 0; j < N; j++) {
				cellRank = (i * N) + j;
				MPI_Send(&(A[i][j]), 1, MPI_INT, cellRank, 0, MPI_COMM_WORLD);
			}
		}
	
		/* All cells are now initialized */
		MPI_Barrier(MPI_COMM_WORLD);
	
		/* Main computation loop */
		startTime = clock();
		for (step = 0; step < N; step++) {
			if (DEBUG) {
				printf("==== step: %d ====\n", step);
			}
			MPI_Barrier(MPI_COMM_WORLD); /* Sync after each step just because. */
		}
		endTime = clock();
		timeTaken = (double) (endTime - startTime) / CLOCKS_PER_SEC;
		printf("N = %d, seed = %d, time taken : %.4f seconds\n", N, seed, timeTaken);
	
		/* Receive the maximum sum from CELL[N - 1][N - 1] */
		MPI_Recv(&maxSumCandidate1, 1, MPI_INT, (((N / 2) - 1) * N) + (N / 2) - 1, 0, MPI_COMM_WORLD, &status);
		MPI_Recv(&maxSumCandidate2, 1, MPI_INT, (((N / 2) - 1) * N) + (N / 2), 0, MPI_COMM_WORLD, &status);
	
		if (maxSumCandidate1 < maxSumCandidate2) {
			SUM_PARALLEL = maxSumCandidate2;
		}
		else {
			SUM_PARALLEL = maxSumCandidate1;
		}
	
		if (DEBUG) {
			SUM_SERIAL = serial(A, N);
			if (SUM_SERIAL != SUM_PARALLEL) {
				printf("FAILED: %d != %d\n", SUM_SERIAL, SUM_PARALLEL);
			}
			else {
				printf("PASSED: %d == %d\n", SUM_SERIAL, SUM_PARALLEL);
			}
		}
	}
	
	/***************************************************************************
	 *
	 * Code for Cells
	 *
	 **************************************************************************/
	else {
		maxSumE = -2 * N;
		maxSumO = -2 * N;
		maxSum = -2 * N;
		solSum = -2 * N;

		ROW = RANK / N;
		COL = RANK % N;

		/* Determine cell rank of neighbour cells */
		RANK_T = (ROW - 1) * N + COL;
		RANK_B = (ROW + 1) * N + COL;
		RANK_L = RANK - 1;
		RANK_R = RANK + 1;

		/* Determine edge cells */
		if (ROW == 0) {
			TOP_EDGE = TRUE;
		}
		else if (ROW == N - 1) {
			BOTTOM_EDGE = TRUE;
		}
		if (COL == 0) {
			LEFT_EDGE = TRUE;
		}
		else if (COL == N - 1) {
			RIGHT_EDGE = TRUE;
		}

		/* Divide the cells into LHS and RHS */
		if (COL < N / 2) {
			LHS = TRUE;
		}
		else {
			RHS = TRUE;
		}

		/* Receive the value of A[ROW][COL] */
		MPI_Recv(&A_VAL, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD, &status);
		MPI_Barrier(MPI_COMM_WORLD);

		/******** MAIN LOOP ********/
		for (step = 0; step < N; step++) {

			/* Send and receive colSum and solSum */
			if (TOP_EDGE) {
				MPI_Sendrecv(&colSumT, 1, MPI_INT, RANK_B, step,
					&colSumBFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_B, step,
					&solSumFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
			}
			else if (BOTTOM_EDGE) {
				MPI_Sendrecv(&colSumB, 1, MPI_INT, RANK_T, step,
					&colSumTFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_T, step,
					&solSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
			}
			else {
				MPI_Sendrecv(&colSumT, 1, MPI_INT, RANK_B, step,
					&colSumBFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&colSumB, 1, MPI_INT, RANK_T, step,
					&colSumTFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_B, step,
					&solSumFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_T, step,
					&solSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
			}

			//printf("%d\n", RANK);

			if (LHS) {
				/* Send recSum, minSum, solSum to the right */
				if (LEFT_EDGE) {
					MPI_Send(&recSumT, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
					MPI_Send(&recSumB, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				}
				else if (COL == (N / 2 - 1)) {
					MPI_Recv(&recSumTFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&recSumBFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				}
				else {
					MPI_Sendrecv(&recSumT, 1, MPI_INT, RANK_R, step,
						&recSumTFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&recSumB, 1, MPI_INT, RANK_R, step,
						&recSumBFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				}
			}
			else {
				/* Send recSum, minSum, solSum to to the left */
				if (RIGHT_EDGE) {
					MPI_Send(&recSumT, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
					MPI_Send(&recSumB, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				}
				else if (COL == (N / 2)) {
					MPI_Recv(&recSumTFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&recSumBFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				}
				else {
					MPI_Sendrecv(&recSumT, 1, MPI_INT, RANK_L, step,
						&recSumTFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&recSumB, 1, MPI_INT, RANK_L, step,
						&recSumBFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				}
			}

			/* First set of computation */
			if (LHS && (COL <= step)) {
				colSumT = colSumTFromT + A_VAL;
				recSumT = recSumTFromL + colSumT;
				colSumB = colSumBFromB + A_VAL;
				recSumBPrev = recSumB;
				recSumB = recSumBFromL + colSumB;
			}
			else if (RHS && (COL >= (N - 1) - step)) {
				colSumT = colSumTFromT + A_VAL;
				recSumT = recSumTFromR + colSumT;
				colSumB = colSumBFromB + A_VAL;
				recSumBPrev = recSumB;
				recSumB = recSumBFromR + colSumB;
			}
		
			/* Second set of data transfer */
			if (TOP_EDGE) {
				MPI_Recv(&recSumBFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&recSumBPrevFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
			}
			else if (BOTTOM_EDGE) {
				MPI_Send(&recSumB, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD);
				MPI_Send(&recSumBPrev, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD);
			}
			else {
				MPI_Sendrecv(&recSumB, 1, MPI_INT, RANK_T, step,
					&recSumBFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				MPI_Sendrecv(&recSumBPrev, 1, MPI_INT, RANK_T, step,
					&recSumBPrevFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
			}
		
			if (LHS) {
				if (LEFT_EDGE) {
					MPI_Send(&minSumE, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
					MPI_Send(&minSumO, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
					MPI_Send(&solSum, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD);
				}
				else if (COL == (N / 2 - 1)) {
					MPI_Recv(&minSumEFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&minSumOFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&solSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				}
				else {
					MPI_Sendrecv(&minSumE, 1, MPI_INT, RANK_R, step,
						&minSumEFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&minSumO, 1, MPI_INT, RANK_R, step,
						&minSumOFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_R, step,
						&solSumFromL, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD, &status);
				}
			}
			else {
				if (RIGHT_EDGE) {
					MPI_Send(&minSumE, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
					MPI_Send(&minSumO, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
					MPI_Send(&solSum, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				}
				else if (COL == (N / 2)) {
					MPI_Recv(&minSumEFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&minSumOFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Recv(&solSumFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				}
				else {
					MPI_Sendrecv(&minSumE, 1, MPI_INT, RANK_L, step,
						&minSumEFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&minSumO, 1, MPI_INT, RANK_L, step,
						&minSumOFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&solSum, 1, MPI_INT, RANK_L, step,
						&solSumFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				}
			}

			/* Second set of computation */
			if (LHS && (COL <= step)) {
				recSumUE = recSumT + recSumBFromB;
				recSumUO = recSumT + recSumBPrevFromB;
				if (minSumEFromL < recSumUE) {
					minSumE = minSumEFromL;
				}
				else {
					minSumE = recSumUE;
				}
				if (minSumOFromL < recSumUO) {
					minSumO = minSumOFromL;
				}
				else {
					minSumO = recSumUO;
				}
				maxSumE = recSumUE - minSumE;
				maxSumO = recSumUO - minSumO;
				if (solSum < solSumFromT) {
					solSum = solSumFromT;
				}
				if (solSum < solSumFromB) {
					solSum = solSumFromB;
				}
				if (solSum < solSumFromL) {
					solSum = solSumFromL;
				}
				if (solSum < maxSumE) {
					solSum = maxSumE;
				}
				if (solSum < maxSumO) {
					solSum = maxSumO;
				}
			}
			else if (RHS && (COL >= (N - 1) - step)) {
				recSumUE = recSumT + recSumBFromB;
				recSumUO = recSumT + recSumBPrevFromB;
				if (minSumEFromR < recSumUE) {
					minSumE = minSumEFromR;
				}
				else {
					minSumE = recSumUE;
				}
				if (minSumOFromR < recSumUO) {
					minSumO = minSumOFromR;
				}
				else {
					minSumO = recSumUO;
				}
				maxSumE = recSumUE - minSumE;
				maxSumO = recSumUO - minSumO;
				if (solSum < solSumFromT) {
					solSum = solSumFromT;
				}
				if (solSum < solSumFromB) {
					solSum = solSumFromB;
				}
				if (solSum < solSumFromR) {
					solSum = solSumFromR;
				}
				if (solSum < maxSumE) {
					solSum = maxSumE;
				}
				if (solSum < maxSumO) {
					solSum = maxSumO;
				}
			}

			/* Third set of data transfer */
			if (COL == (N / 2)) {
				MPI_Send(&maxSumE, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
				MPI_Send(&maxSumO, 1, MPI_INT, RANK_L, step, MPI_COMM_WORLD);
			}
			else if (COL == (N / 2) - 1) {
				MPI_Recv(&maxSumEFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
				MPI_Recv(&maxSumOFromR, 1, MPI_INT, RANK_R, step, MPI_COMM_WORLD, &status);
			}
			if (COL == (N / 2) - 1) {
				if (TOP_EDGE) {
					MPI_Sendrecv(&midSum, 1, MPI_INT, RANK_B, step,
						&midSumFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
				}
				else if (BOTTOM_EDGE) {
					MPI_Sendrecv(&midSum, 1, MPI_INT, RANK_T, step,
						&midSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				}
				else {
					MPI_Sendrecv(&midSum, 1, MPI_INT, RANK_B, step,
						&midSumFromB, 1, MPI_INT, RANK_B, step, MPI_COMM_WORLD, &status);
					MPI_Sendrecv(&midSum, 1, MPI_INT, RANK_T, step,
						&midSumFromT, 1, MPI_INT, RANK_T, step, MPI_COMM_WORLD, &status);
				}
			
				midSumE = maxSumE + maxSumEFromR;
				midSumO = maxSumO + maxSumOFromR;
				if (midSum < midSumE) {
					midSum = midSumE;
				}
				if (midSum < midSumO) {
					midSum = midSumO;
				}
				if (midSum < midSumFromT) {
					midSum = midSumFromT;
				}
				if (midSum < maxSumFromB) {
					midSum = midSumFromB;
				}
				if (solSum < midSum) {
					solSum = midSum;
				}
			}

			MPI_Barrier(MPI_COMM_WORLD); /* Sync each step */
		}
	
		if ((ROW == (N / 2) - 1) && ((COL == (N / 2) - 1))) {
			MPI_Send(&solSum, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD);
		}
		if ((ROW == (N / 2) - 1) && (COL == (N / 2))) {
			MPI_Send(&solSum, 1, MPI_INT, MASTER, 0, MPI_COMM_WORLD);
		}
	}
	
	MPI_Finalize();
	
	return 0;
}



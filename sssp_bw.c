#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define DEBUG 1

#define TRUE 1
#define FALSE 0

typedef struct _EDGE {
	unsigned int src;
	unsigned int dst;
	float cap;
} EDGE;

typedef struct _NODE {
	unsigned int id;
	unsigned int numEdges;
	float curCap;
	EDGE** edges;
	struct _NODE* parent;
	struct _NODE_DLL* children;
} NODE;

typedef struct _NODE_DLL {
	struct _NODE_DLL* prev;
	struct _NODE_DLL* next;
	NODE* node;
} NODE_DLL;

typedef struct {
	unsigned int dist;
	unsigned int* path;
} PATH;

static float** C;
static float** F;
static unsigned int NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS;
static NODE** NODES;





/******************************************************************************/

void simple_algorithm(PATH* PATH) {

	int i;
	unsigned int SRC;
	unsigned int dst;
	unsigned char* inQueue;
	NODE* node;
	NODE* parent;
	NODE* child;
	NODE_DLL* Q_HEAD;
	NODE_DLL* Q_TAIL;
	NODE_DLL* nodeDLL;

	SRC = 0;

	inQueue = (unsigned char*) malloc(NUM_NODES * sizeof(unsigned char));
	for (dst = 0; dst < NUM_NODES; dst++) {
		if (dst != SRC) {
			for (i = 0; i < NUM_NODES; i++) {
				inQueue[i] = FALSE;
			}
			inQueue[SRC] = TRUE;
			nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
			nodeDLL->node = NODES[SRC];
			Q_HEAD = nodeDLL;
			Q_HEAD->next = NULL;
			Q_HEAD->prev = NULL;
			Q_TAIL = Q_HEAD;
			while (Q_HEAD != NULL) {
				/* Pop the first item from the Q */
				nodeDLL = Q_HEAD;
				Q_HEAD = nodeDLL->next;
				if (Q_HEAD) {
					Q_HEAD->prev = NULL;
				}
				nodeDLL->next = NULL;
				nodeDLL->prev = NULL;
				/* For all EDGEs from this node */
				node = nodeDLL->node;
				for (i = 0; i < node->numEdges; i++) {
					if (F[SRC][dst] <= node->edges[i]->cap) {
						if (!inQueue[node->edges[i]->dst]) {
							child = NODES[node->edges[i]->dst];
							child->parent = node;
							if (child->id == dst) {
								/* Destination node found */
								parent = child->parent;
								while (parent != NULL) {
									PATH[dst].dist++;
									PATH[dst].path = realloc(PATH[dst].path, (PATH[dst].dist * sizeof(unsigned int)));
									PATH[dst].path[PATH[dst].dist - 1] = parent->id;
									parent = parent->parent;
								}
								Q_HEAD = NULL;
								break;
							}
							nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
							nodeDLL->node = child;
							nodeDLL->next = NULL;
							nodeDLL->prev = Q_TAIL;
							Q_TAIL->next = nodeDLL;
							Q_TAIL = nodeDLL;
							if (Q_HEAD == NULL) {
								Q_HEAD = nodeDLL;
							}
							inQueue[child->id] = TRUE;
						}
					}
				}
			}
		}
	}
}





/******************************************************************************/

void shinn_algorithm(PATH* PATH) {

	int i, j, n, m;
	unsigned int SRC;
	unsigned int srcId, dstId;
	unsigned int numEdges, numNodes;
	unsigned int child, parent, leftChild, rightChild;
	EDGE** EDGE_HEAP;
	EDGE* edge;
	NODE** NODE_HEAP;
	NODE* node;
	NODE* maxNode;
	NODE* dstNode;
	NODE_DLL* Q_HEAD;
	NODE_DLL* Q_TAIL;
	NODE_DLL* nodeDLL;
	NODE_DLL* maxNodeDLL;
	NODE_DLL* dstNodeDLL;
	unsigned char* inQueue;

	SRC = 0;

	/* Initialize 1D arrays to be used as heaps (for sorting edges and nodes) */
	EDGE_HEAP = (EDGE**) malloc(NUM_EDGES * sizeof(EDGE*));
	NODE_HEAP = (NODE**) malloc(NUM_NODES * sizeof(NODE*));

	/* Add each edge to the heap. */
	numEdges = 0;
	for (i = 0; i < NUM_NODES; i++) {
		for (j = 0; j < NODES[i]->numEdges; j++) {
			EDGE_HEAP[numEdges] = NODES[i]->edges[j];
			child = numEdges;
			while (child > 0) {
				parent = (child - 1) / 2;
				if (EDGE_HEAP[parent]->cap < EDGE_HEAP[child]->cap) {
					edge = EDGE_HEAP[parent];
					EDGE_HEAP[parent] = EDGE_HEAP[child];
					EDGE_HEAP[child] = edge;
				}
				child = parent;
			}
			numEdges++;
		}
	}
	
	/* Turn the edge heap into a sorted array */
	for (i = (numEdges - 1); i > 0; i--) {
		parent = 0;
		edge = EDGE_HEAP[i];
		EDGE_HEAP[i] = EDGE_HEAP[parent];
		EDGE_HEAP[parent] = edge;
		while (((parent * 2) + 1) <= (i - 1)) {
			child = parent;
			leftChild = (parent * 2) + 1;
			rightChild = leftChild + 1;
			if (EDGE_HEAP[parent]->cap < EDGE_HEAP[leftChild]->cap) {
				child = leftChild;
			}
			if ((rightChild <= (i - 1)) && (EDGE_HEAP[child]->cap < EDGE_HEAP[rightChild]->cap)) {
				child = rightChild;
			}
			if (child != parent) {
				edge = EDGE_HEAP[parent];
				EDGE_HEAP[parent] = EDGE_HEAP[child];
				EDGE_HEAP[child] = edge;
				parent = child;
			}
			else {
				break;
			}
		}
	}

	/* Add each node to the heap */
	numNodes = 0;
	for (i = 0; i < NUM_NODES; i++) {
		if (i != SRC) {
			NODE_HEAP[numNodes] = NODES[i];
			child = numNodes;
			while (child > 0) {
				parent = (child - 1) / 2;
				if (F[SRC][NODE_HEAP[parent]->id] < F[SRC][NODE_HEAP[child]->id]) {
					node = NODE_HEAP[parent];
					NODE_HEAP[parent] = NODE_HEAP[child];
					NODE_HEAP[child] = node;
				}
				child = parent;
			}
			numNodes++;
		}
	}

	/* Turn the node heap into a sorted array */
	for (i = (numNodes - 1); i > 0; i--) {
		parent = 0;
		node = NODE_HEAP[i];
		NODE_HEAP[i] = NODE_HEAP[parent];
		NODE_HEAP[parent] = node;
		while (((parent * 2) + 1) <= (i - 1)) {
			child = parent;
			leftChild = (parent * 2) + 1;
			rightChild = leftChild + 1;
			if (F[SRC][NODE_HEAP[parent]->id] < F[SRC][NODE_HEAP[leftChild]->id]) {
				child = leftChild;
			}
			if ((rightChild <= (i - 1)) && (F[SRC][NODE_HEAP[child]->id] < F[SRC][NODE_HEAP[rightChild]->id])) {
				child = rightChild;
			}
			if (child != parent) {
				node = NODE_HEAP[parent];
				NODE_HEAP[parent] = NODE_HEAP[child];
				NODE_HEAP[child] = node;
				parent = child;
			}
			else {
				break;
			}
		}
	}

	printf("\nSorted Edges\n");
	for (i = 0; i < numEdges; i++) {
		printf("%.2f ", EDGE_HEAP[i]->cap);
	}
	printf("\n");

	printf("\nSorted Nodes\n");
	for (i = 0; i < numNodes; i++) {
		printf("%.2f (%d) ", F[SRC][NODE_HEAP[i]->id], NODE_HEAP[i]->id);
	}
	printf("\n");
	
	
	
	/* Build the initial MMT */
	inQueue = (unsigned char*) malloc(NUM_NODES * sizeof(unsigned char));
	for (i = 0; i < NUM_NODES; i++) {
		inQueue[i] = FALSE;
	}
	NODES[SRC]->curCap = MAX_CAP;
	nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
	nodeDLL->node = NODES[SRC];
	nodeDLL->prev = NULL;
	nodeDLL->next = NULL;
	Q_HEAD = nodeDLL;
	inQueue[SRC] = TRUE;
	while (Q_HEAD != NULL) {
		/* Remove the maxNode in the queue with the highest capacity */
		nodeDLL = Q_HEAD;
		maxNodeDLL = nodeDLL;
		while (nodeDLL != NULL) {
			if (maxNodeDLL->node->curCap < nodeDLL->node->curCap) {
				maxNodeDLL = nodeDLL;
			}
			nodeDLL = nodeDLL->next;
		}
		if (maxNodeDLL->prev != NULL) {
			maxNodeDLL->prev->next = maxNodeDLL->next;
		}
		else {
			Q_HEAD = maxNodeDLL->next;
		}
		if (maxNodeDLL->next != NULL) {
			maxNodeDLL->next->prev = maxNodeDLL->prev;
		}

		/* Re-use the DLL structure when adding maxNode to list of children of parent node */
		maxNode = maxNodeDLL->node;
		if (maxNode->parent != NULL) {
			node = maxNode->parent;
			if (node->children != NULL) {
				node->children->prev = maxNodeDLL;
			}
			maxNodeDLL->next = node->children;
			maxNodeDLL->prev = NULL;
			node->children = maxNodeDLL;
		}
		inQueue[maxNode->id] = FALSE;

		/* For all nodes reachable from maxNode... */
		for (i = 0; i < maxNode->numEdges; i++) {
			node = NODES[maxNode->edges[i]->dst];
			if (inQueue[node->id]) {
				/* already in Q, update parent if new capacity is larger */
				if (node->curCap < maxNode->edges[i]->cap) {
					node->curCap = maxNode->edges[i]->cap;
					node->parent = maxNode;
				}
			}
			else if (!node->curCap) {
				/* not already in Q and not yet finalized, so add to Q */
				node->parent = maxNode;
				node->curCap = maxNode->edges[i]->cap;
				nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
				nodeDLL->node = node;
				if (Q_HEAD != NULL) {
					Q_HEAD->prev = nodeDLL;
				}
				nodeDLL->next = Q_HEAD;
				nodeDLL->prev = NULL;
				Q_HEAD = nodeDLL;
				inQueue[node->id] = TRUE;
			}
		}
	}

	
	/* Start main loop */

	n = numNodes - 1;
	for (m = numEdges - 1; m >= 0; m--) {

		printf("\n");
		printf("================ m: %d (%.2f), n: %d (%.2f)\n", m, EDGE_HEAP[m]->cap, n, F[SRC][NODE_HEAP[n]->id]);
		for (i = 0; i < NUM_NODES; i++) {
			printf("NODE[%d]: ", i);
			nodeDLL = NODES[i]->children;
			while (nodeDLL != NULL) {
				printf("%d ", nodeDLL->node->id);
				nodeDLL = nodeDLL->next;
			}
			printf("\n");
		}
		
		i = n;
		while (F[SRC][NODE_HEAP[i]->id] > EDGE_HEAP[m]->cap) {
			i--;
			if (i < 0) {
				break;
			}
		}

		if (i < n) {
			printf("i: %d, n: %d\n", i, n);
			/* BFS on MMT */
			for (j = 0; j < NUM_NODES; j++) {
				inQueue[j] = FALSE;
			}
			inQueue[SRC] = TRUE;
			nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
			nodeDLL->node = NODES[SRC];
			nodeDLL->next = NULL;
			nodeDLL->prev = NULL;
			Q_HEAD = nodeDLL;
			Q_TAIL = nodeDLL;
			while (Q_HEAD != NULL) {
				/* Pop the first item from the Q */
				nodeDLL = Q_HEAD;
				Q_HEAD = nodeDLL->next;
				if (Q_HEAD != NULL) {
					Q_HEAD->prev = NULL;
				}
				else {
					Q_TAIL = NULL;
				}
				nodeDLL->next = NULL;
				nodeDLL->prev = NULL;
				node = nodeDLL->node;
				
				/* For all current children of this node... */
				nodeDLL = node->children;
				while (nodeDLL != NULL) {
					dstNode = nodeDLL->node;
					if (!inQueue[dstNode->id]) {
						/* Not already in queue so add to the end of the queue */
						dstNode->parent = node;
						dstNodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
						dstNodeDLL->node = dstNode;
						dstNodeDLL->next = NULL;
						dstNodeDLL->prev = Q_TAIL;
						if (Q_TAIL != NULL) {
							Q_TAIL->next = dstNodeDLL;
						}
						else {
							Q_HEAD = dstNodeDLL;
						}
						Q_TAIL = dstNodeDLL;
						inQueue[dstNode->id] = TRUE;
					}
					else {
						/* already in queue, remove from children list of parent node */
						if (nodeDLL->prev != NULL) {
							nodeDLL->prev->next = nodeDLL->next;
						}
						else {
							node->children = nodeDLL->next;
						}
						if (nodeDLL->next != NULL) {
							nodeDLL->next->prev = nodeDLL->prev;
						}
					}
					nodeDLL = nodeDLL->next;
				}
			}


			for (j = n; j > i; j--) {
				printf("Finalize Node %d\n", NODE_HEAP[j]->id);
				dstId = NODE_HEAP[j]->id;
				node = NODE_HEAP[j]->parent;
				while (node != NULL) {
					PATH[dstId].dist++;
					PATH[dstId].path = realloc(PATH[dstId].path, (PATH[dstId].dist * sizeof(unsigned int)));
					PATH[dstId].path[PATH[dstId].dist - 1] = node->id;
					node = node->parent;
				}
			}
			n = i;
		}

		if (i < 0) {
			break;
		}
		
		/* Add EDGE_HEAP[m] to MMT */
		dstId = EDGE_HEAP[m]->dst;
		if (dstId != SRC) {
			srcId = EDGE_HEAP[m]->src;
			printf("SrcID: %d, DstID: %d, ParentID: %d\n", srcId, dstId, NODES[dstId]->parent->id);
			if (NODES[dstId]->parent->id != srcId) {
				nodeDLL = (NODE_DLL*) malloc(sizeof(NODE_DLL));
				nodeDLL->node = NODES[dstId];
				nodeDLL->prev = NULL;
				if (NODES[srcId]->children != NULL) {
					NODES[srcId]->children->prev = nodeDLL;
				}
				nodeDLL->next = NODES[srcId]->children;
				NODES[srcId]->children = nodeDLL;
			}
		}
	}
}





/******************************************************************************/

int main(int argc, char** argv) {

	unsigned int i, j;
	unsigned int runNum;
	float* tmp;
	unsigned int seed;
	unsigned int numEdges;
	PATH* PATH1;
	PATH* PATH2;
	clock_t startTime, endTime;
	double timeSimple, timeShinn;
	EDGE* edge;

	if (argc != 5) {
		printf("Usage: %s <NUM_NODES> <MULTIPLIER> <MAX_CAP> <NUM_RUNS>\n", argv[0]);
		return 0;
	}

	NUM_NODES = atoi(argv[1]);
	NUM_EDGES = atoi(argv[2]) * NUM_NODES;
	if (NUM_EDGES > ((NUM_NODES * NUM_NODES) - NUM_NODES)) {
		NUM_EDGES = (NUM_NODES * NUM_NODES) - NUM_NODES;
	}
	MAX_CAP   = atoi(argv[3]);
	NUM_RUNS  = atoi(argv[4]);
	seed = time(NULL);
	srand(seed);
	printf("\nNUM_NODES = %d, NUM_EDGES = %d, MAX_CAP = %d, NUM_RUNS = %d, seed = %d\n", NUM_NODES, NUM_EDGES, MAX_CAP, NUM_RUNS, seed);

	/* Initialize 2D arrays for storing Edge capacities and Flow requirements */
	C = (float**) malloc(NUM_NODES * sizeof(float*));
	tmp = (float*) malloc(NUM_NODES * NUM_NODES * sizeof(float));
	for (i = 0; i < NUM_NODES; i++) {
		C[i] = tmp + (i * NUM_NODES);
	}
	F = (float**) malloc(NUM_NODES * sizeof(float*));
	tmp = (float*) malloc(NUM_NODES * NUM_NODES * sizeof(float));
	for (i = 0; i < NUM_NODES; i++) {
		F[i] = tmp + (i * NUM_NODES);
	}

	/* Initialize 1D array of pointers to NODEs and array of PATHs*/
	NODES = (NODE**) malloc(NUM_NODES * sizeof(NODE*));
	for (i = 0; i < NUM_NODES; i++) {
		NODES[i] = (NODE*) malloc(sizeof(NODE));
	}
	PATH1 = (PATH*) malloc(NUM_NODES * sizeof(PATH));
	PATH2 = (PATH*) malloc(NUM_NODES * sizeof(PATH));

	/*********************
	 ***** MAIN LOOP *****
	 *********************/
	for (runNum = 0; runNum < NUM_RUNS; runNum++) {
	
		/* Generate random edge capacities and flow requirements */
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				C[i][j] = 0;
				F[i][j] = (((float) rand() / (float) RAND_MAX) * MAX_CAP);
			}
		}
		numEdges = 0;
		while (numEdges < NUM_EDGES) {
			i = rand() % NUM_NODES;
			j = rand() % NUM_NODES;
			if (i != j) {
				if (C[i][j] == 0) {
					C[i][j] = (((float) rand() / (float) RAND_MAX) + 0) * MAX_CAP;
					numEdges++;
				}
			}
		}

#ifdef DEBUG
		printf("\n======== Edge Capacities ========\n");
		for (i = 0; i < NUM_NODES; i++) {
			for(j = 0; j < NUM_NODES; j++) {
				printf("%.2f ", C[i][j]);
			}
			printf("\n");
		}
		printf("\n======== Flow Requirements ========\n");
		for (i = 0; i < NUM_NODES; i++) {
			printf("%.2f ", F[0][i]);
		}
		printf("\n");
#endif

		/* Initialize data structures */
		for (i = 0; i < NUM_NODES; i++) {
			NODES[i]->id = i;
			NODES[i]->numEdges = 0;
			NODES[i]->curCap = 0;
			NODES[i]->edges = NULL;
			NODES[i]->parent = NULL;
			NODES[i]->children = NULL;
			PATH1[i].dist = 0;
			PATH1[i].path = NULL;
			PATH2[i].dist = 0;
			PATH2[i].path = NULL;
		}
		for (i = 0; i < NUM_NODES; i++) {
			for (j = 0; j < NUM_NODES; j++) {
				if (C[i][j] > 0) {
					edge = (EDGE*) malloc(sizeof(EDGE));
					edge->src = i;
					edge->dst = j;
					edge->cap = C[i][j];
					NODES[i]->numEdges++;
					NODES[i]->edges = realloc(NODES[i]->edges, NODES[i]->numEdges * sizeof(EDGE*));
					NODES[i]->edges[NODES[i]->numEdges - 1] = edge;
				}
			}
		}

		/* Solve using the simple algorithm */
		startTime = clock();
		simple_algorithm(PATH1);
		endTime = clock();
		timeSimple = (double) (endTime - startTime) / CLOCKS_PER_SEC;

		/* Clean NODES for the next algorithm */
		for (i = 0; i < NUM_NODES; i++) {
			NODES[i]->curCap = 0;
			NODES[i]->parent = NULL;
			NODES[i]->children = NULL;
		}

		/* Solve using Shinn's algorithm */
		startTime = clock();
		shinn_algorithm(PATH2);
		endTime = clock();
		timeShinn = (double) (endTime - startTime) / CLOCKS_PER_SEC;

		/* Shinn's algorithm gives best effort answer for nodes that can't be satisified */
		for (i = 0; i < NUM_NODES; i++) {
			if (PATH2[i].dist != 0) {
				if (C[PATH2[i].path[0]][i] < F[0][i]) {
					PATH2[i].dist = 0;
				}
				else {
					for (j = 0; j < PATH2[i].dist - 1; j++) {
						if (C[PATH2[i].path[j + 1]][PATH2[i].path[j]] < F[0][i]) {
							PATH2[i].dist = 0;
							break;
						}
					}
				}
			}
		}

		/* Check that the two results are the same */
		printf("\n==== Simple Algorithm ====\n");
		for (i = 0; i < NUM_NODES; i++) {
			if (PATH1[i].dist == 0) {
				printf("%d\n", i);
			}
			else {
				printf("%d <- (%.2f) ", i, C[PATH1[i].path[0]][i]);
				for (j = 0; j < PATH1[i].dist - 1; j++) {
					printf("<- %d <- (%.2f) ", PATH1[i].path[j], C[PATH1[i].path[j + 1]][PATH1[i].path[j]]);
				}
				printf("<- 0\n");
			}
		}
		printf("\n==== Shinn's Algorithm ====\n");
		for (i = 0; i < NUM_NODES; i++) {
			if (PATH2[i].dist == 0) {
				printf("%d\n", i);
			}
			else {
				printf("%d <- (%.2f) ", i, C[PATH2[i].path[0]][i]);
				for (j = 0; j < PATH2[i].dist - 1; j++) {
					printf("<- %d <- (%.2f) ", PATH2[i].path[j], C[PATH2[i].path[j + 1]][PATH2[i].path[j]]);
				}
				printf("<- 0\n");
			}
		}
		
		/*
		for (i = 1; i < NUM_NODES; i++) {
			printf("PATH[0][%d] (%d) : ", i, PATH2[i].dist);
			for (j = 0; j < PATH2[i].dist; j++) {
				printf("%d ", PATH2[i].path[j]);
			}
			printf("\n");
		}
		*/
		
		printf("\n");
		printf("Simple Algorithm  : %.2f seconds\n", timeSimple);
		printf("Shinn's Algorithm : %.2f seconds\n", timeShinn);
	}
	
	return 0;
}

